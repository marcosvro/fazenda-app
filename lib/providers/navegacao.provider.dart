import 'package:flutter/material.dart';

const INDEX_HOME = "home";
const INDEX_LOGIN = "login";
const INDEX_FAZENDA = "fazenda";

class NavegacaoHomeProvider extends ChangeNotifier {
  String index;

  NavegacaoHomeProvider() {
    setNavegacao(INDEX_HOME);
  }

  setNavegacao(String servico) {
    index = servico;
    notifyListeners();
  }
}
