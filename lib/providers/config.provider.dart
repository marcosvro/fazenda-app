import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

const INDEX_LOGIN_ACCOUNT = "login-account";
const INDEX_CREATE_ACCOUNT = "create-account";

const CONFIG_THEME_MODE_PREFS_KEY = "config_theme";

enum ThemeMode { DARK, LIGHT }

class ConfigProvider extends ChangeNotifier {
  ThemeMode themeMode;

  ConfigProvider() {
    loadPreferences();
  }

  loadPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String themeModeString = prefs.getString(CONFIG_THEME_MODE_PREFS_KEY);
    if (themeModeString != null) {
      if (themeModeString == ThemeMode.DARK.toString()) {
        this.themeMode = ThemeMode.DARK;
      } else {
        this.themeMode = ThemeMode.LIGHT;
      }
    } else {
      await setThemeMode(ThemeMode.LIGHT, notify: false);
    }
    notifyListeners();
  }

  setThemeMode(ThemeMode mode, {bool notify = true}) async {
    this.themeMode = mode;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (mode != null) {
      prefs.setString(CONFIG_THEME_MODE_PREFS_KEY, mode.toString());
    }
    if (notify) {
      notifyListeners();
    }
  }
}
