import 'package:app_fazenda/blocs/bloc_auth.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:flutter/material.dart';

const INDEX_LOGIN_ACCOUNT = "login-account";
const INDEX_CREATE_ACCOUNT = "create-account";

class AuthProvider extends ChangeNotifier {
  AuthBloc _bloc;
  String index;

  AuthProvider() {
    index = INDEX_LOGIN_ACCOUNT;
    _bloc = AuthBloc();
    _bloc.loadPreferences();
  }

  AuthBloc get bloc => _bloc;

  setNavegacao(String servico) {
    index = servico;
    notifyListeners();
  }
}
