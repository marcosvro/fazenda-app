import 'package:app_fazenda/blocs/bloc_auth.dart';
import 'package:app_fazenda/blocs/bloc_leitora.dart';
import 'package:flutter/material.dart';

const INDEX_LOGIN_ACCOUNT = "login-account";
const INDEX_CREATE_ACCOUNT = "create-account";

class LeitoraProvider extends ChangeNotifier {
  LeitoraBloc _bloc;
  String index;

  LeitoraProvider() {
    _bloc = LeitoraBloc();
  }

  LeitoraBloc get bloc => _bloc;
}
