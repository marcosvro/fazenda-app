import 'package:app_fazenda/blocs/bloc_paginated_list.dart';
import 'package:app_fazenda/blocs/bloc_search.dart';
import 'package:flutter/material.dart';

class Filter {
  final String label;
  final String searchParamKey;

  Filter({@required this.label, @required this.searchParamKey});
}

class SearchProvider extends ChangeNotifier {
  SearchQuery _searchQuery;
  String _activeForm;
  String _searchFilterKey;
  PaginatedListBloc _listBloc;

  Map<String, List<Filter>> optionsFilters = {
    'animal_form': <Filter>[
      Filter(label: 'Nome', searchParamKey: 'name'),
      Filter(label: 'Número do brinco', searchParamKey: 'numero_brinco'),
      Filter(label: 'Número do chip', searchParamKey: 'numero_chip'),
    ],
    'lote_form': <Filter>[
      Filter(label: 'Nome', searchParamKey: 'name'),
    ],
  };

  SearchProvider() {
    _searchQuery = new SearchQuery(key: null, value: null);
    this.setActiveForm('animal_form');
  }

  PaginatedListBloc get listBloc => _listBloc;
  SearchQuery get searchQuery => _searchQuery;
  String get activeForm => _activeForm;
  String get searchFilterKey => _searchFilterKey;

  setListBloc<T>(Future<List<T>> Function(PageConfig, SearchQuery) getElemets) {
    if (this._listBloc != null) {
      this._listBloc.dispose();
    }
    this._listBloc = new PaginatedListBloc<T>(getElemets);
    this._listBloc.fetchList(query: this.searchQuery);
    notifyListeners();
  }

  setSearchQuery({String key, String value, bool updateList = false}) {
    bool hasModify = false;
    if (key != null) {
      this._searchQuery.key = key;
      hasModify = true;
    }
    if (value != null) {
      this._searchQuery.value = value;
      hasModify = true;
    }

    if (hasModify && updateList && this._listBloc != null) {
      this._listBloc.fetchList(query: this.searchQuery);
    }
  }

  setActiveForm(String form) {
    this._activeForm = form;
    this.setSearchFilterKey(optionsFilters[form][0].searchParamKey);
    notifyListeners();
  }

  setSearchFilterKey(String key, {bool updateList = false}) {
    this._searchFilterKey = key;
    this.setSearchQuery(key: key, updateList: updateList);
  }

  disposeListBloc() {
    this._listBloc.dispose();
    this._listBloc = null;
  }
}
