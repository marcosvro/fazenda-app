import 'package:app_fazenda/providers/search.provider.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuscaBarra extends StatefulWidget {
  const BuscaBarra({Key key}) : super(key: key);

  @override
  _BuscaBarraState createState() => _BuscaBarraState();
}

class _BuscaBarraState extends State<BuscaBarra>
    with SingleTickerProviderStateMixin {
  bool _isExpanded = false;
  Animation<double> _iconTurns;
  AnimationController _controller;
  static final Animatable<double> _halfTween =
      Tween<double>(begin: 0.0, end: 0.5);
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);

  TextEditingController _searchTextController = new TextEditingController();

  bool _hasText = false;

  @override
  void initState() {
    _controller =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
    _iconTurns = _controller.drive(_halfTween.chain(_easeInTween));

    _searchTextController.addListener(() {
      setState(() {
        _hasText = _searchTextController.text.length != 0;
      });
    });

    super.initState();
  }

  void swapOptions() {
    _isExpanded = !_isExpanded;
    if (_isExpanded) {
      _controller.forward();
    } else {
      _controller.reverse().then<void>((void value) {
        if (!mounted) return;
        setState(() {
          // Rebuild without children.
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SearchProvider>(
      builder: (context, provider, child) {
        return Card(
          child: Column(
            children: [
              ExpansionTile(
                initiallyExpanded: _isExpanded,
                onExpansionChanged: (isExpanded) => this.swapOptions(),
                tilePadding: EdgeInsets.all(0.0),
                title: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    if (_hasText)
                      IconButton(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        icon: Icon(
                          Icons.close,
                          color: PRIMARY_SWATCH,
                        ),
                        onPressed: () {
                          _searchTextController.clear();
                          provider.setSearchQuery(
                            value: _searchTextController.text,
                            updateList: true,
                          );
                        },
                      ),
                    Expanded(
                      child: TextFormField(
                        textAlign: TextAlign.start,
                        autovalidate: false,
                        controller: _searchTextController,
                        autocorrect: false,
                        maxLines: 1,
                        autofocus: false,
                        enableInteractiveSelection: true,
                        decoration: InputDecoration(
                          fillColor: Colors.transparent,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Digite algo para buscar',
                        ),
                        keyboardType: TextInputType.text,
                        onEditingComplete: () => provider.setSearchQuery(
                          value: _searchTextController.text,
                          updateList: true,
                        ),
                      ),
                    ),
                  ],
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.search,
                        color: PRIMARY_SWATCH,
                      ),
                      onPressed: () => provider.setSearchQuery(
                        value: _searchTextController.text,
                        updateList: true,
                      ),
                    ),
                    RotationTransition(
                      turns: _iconTurns,
                      child: SizedBox(
                        width: 60,
                        child: const Icon(
                          Icons.arrow_drop_down,
                          color: PRIMARY_SWATCH,
                        ),
                      ),
                    ),
                  ],
                ),
                children: [
                  Divider(),
                  Padding(
                    padding:
                        const EdgeInsets.only(right: 20, left: 20, bottom: 10),
                    child: DropDownFormField(
                      titleText: 'Buscar por:',
                      hintText: 'Oque quer buscar?',
                      value: provider.activeForm,
                      onSaved: (value) {
                        provider.setActiveForm(value);
                      },
                      onChanged: (value) {
                        provider.setActiveForm(value);
                      },
                      dataSource: [
                        {
                          "display": "Animal",
                          "value": "animal_form",
                        },
                        {
                          "display": "Lote",
                          "value": "lote_form",
                        },
                      ],
                      textField: 'display',
                      valueField: 'value',
                    ),
                  ),
                  if (provider.activeForm != null)
                    Padding(
                      padding: EdgeInsets.only(right: 20, left: 20, bottom: 10),
                      child: DropDownFormField(
                        titleText: 'Filtrar por:',
                        hintText: 'Por qual campo deseja buscar?',
                        value: provider.searchFilterKey,
                        onSaved: (value) {
                          provider.setSearchFilterKey(value, updateList: true);
                        },
                        onChanged: (value) {
                          provider.setSearchFilterKey(value, updateList: true);
                        },
                        dataSource: List.generate(
                            provider.optionsFilters[provider.activeForm].length,
                            (index) => {
                                  "display": provider
                                      .optionsFilters[provider.activeForm]
                                          [index]
                                      .label,
                                  "value": provider
                                      .optionsFilters[provider.activeForm]
                                          [index]
                                      .searchParamKey,
                                }),
                        textField: 'display',
                        valueField: 'value',
                      ),
                    ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
