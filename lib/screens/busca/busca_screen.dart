// WIDGET CHECK
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/search.provider.dart';
import 'package:app_fazenda/repositories/animal_repository.dart';
import 'package:app_fazenda/repositories/lote_repository.dart';
import 'package:app_fazenda/screens/busca/busca_barra.dart';
import 'package:app_fazenda/screens/busca/list_animal.dart';
import 'package:app_fazenda/screens/busca/list_lote.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuscaScreen extends StatefulWidget {
  @override
  _BuscaScreenState createState() => _BuscaScreenState();
}

class _BuscaScreenState extends State<BuscaScreen> {
  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);

    return StreamBuilder<ApiResponse<Fazenda>>(
      stream: authProvider.bloc.fazenda,
      builder: (context, snapshot) {
        var loadingContent = Center(
          child: CircularProgressIndicator(),
        );

        if (snapshot.hasData && snapshot.data.status == Status.COMPLETED) {
          Fazenda fazenda = snapshot.data.data;
          return BuscarNaFazenda(
            fazenda: fazenda,
          );
        } else {
          return loadingContent;
        }
      },
    );
  }
}

// WIDGET CONTENT
class BuscarNaFazenda extends StatefulWidget {
  final Fazenda fazenda;

  BuscarNaFazenda({@required this.fazenda});

  @override
  _BuscarNaFazendaState createState() => _BuscarNaFazendaState();
}

class _BuscarNaFazendaState extends State<BuscarNaFazenda> {
  SearchProvider _provider = SearchProvider();
  AnimalRepository animalRepository = AnimalRepository();
  LoteRepository loteRepository = LoteRepository();

  Map<String, Widget Function()> searchLists = {
    'animal_form': () => ListAnimal(),
    'lote_form': () => ListLote()
  };

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _provider,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: BuscaBarra(),
          ),
          SizedBox(height: 10),
          Consumer<SearchProvider>(
            builder: (context, provider, child) {
              if (provider.activeForm != null)
                return Expanded(
                  child: searchLists[provider.activeForm](),
                );
              else
                return SizedBox(height: 0);
            },
          ),
        ],
      ),
    );
  }
}
