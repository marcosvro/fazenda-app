import 'package:app_fazenda/blocs/bloc_paginated_list.dart';
import 'package:app_fazenda/blocs/bloc_search.dart';
import 'package:app_fazenda/models/lote.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/providers/search.provider.dart';
import 'package:app_fazenda/repositories/lote_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListLote extends StatefulWidget {
  const ListLote({Key key}) : super(key: key);

  @override
  _ListLoteState createState() => _ListLoteState();
}

class _ListLoteState extends State<ListLote> {
  LoteRepository loteRepository = LoteRepository();

  final RefreshController _refreshController = RefreshController(
    initialRefresh: false,
  );

  bool _loading = false;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      SearchProvider searchProvider = Provider.of(context, listen: false);
      searchProvider.setListBloc<Lote>(this.getLoteList);
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
    try {
      SearchProvider searchProvider = Provider.of(context, listen: false);
      searchProvider.disposeListBloc();
    } catch (e) {}
  }

  Future<List<Lote>> getLoteList(
      PageConfig pageConfig, SearchQuery searchQuery) async {
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    try {
      var lotes = await loteRepository.getLotes(
        fazendaID: authProvider.bloc.fazendaObject?.id,
        token: authProvider.bloc.accessTokenObject,
        page: pageConfig?.page,
        pageSize: pageConfig?.size,
        queryKey: searchQuery?.key,
        queryValue: searchQuery?.value,
      );

      return lotes;
    } catch (e) {
      print(e);
      String error;
      if (e.runtimeType == ServiceHandableException) {
        ServiceHandableException exception = (e as ServiceHandableException);
        error = exception.message;
        if (exception.message.contains("TOKEN_EXPIRED")) {
          error = "Token expirado faça login.. por favor faça login novamente.";
          authProvider.bloc.changeAccessToken(ApiResponse.error(""));
          var navegacaoProvider =
              Provider.of<NavegacaoHomeProvider>(context, listen: false);
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      } else {
        error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
      }
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
      return null;
    }
  }

  List<Widget> _getWidgets(List<Lote> list) {
    var labelStyle = TextStyle(fontSize: 12);
    return list
        .map<Widget>((e) => Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                        child: Center(
                            child: Text(e.nome ?? "-", style: labelStyle))),
                    Expanded(
                        child: Center(
                            child: Text(
                                e.producao != null
                                    ? (e.producao ? "Sim" : "Não")
                                    : "-",
                                style: labelStyle))),
                  ],
                ),
                Divider(),
              ],
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    SearchProvider searchProvider = Provider.of(context, listen: false);

    const labelStyle = TextStyle(fontSize: 12, color: PRIMARY_SWATCH);

    if (searchProvider.listBloc != null &&
        searchProvider.listBloc.list is Stream<List<Lote>>) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(child: Center(child: Text("Nome", style: labelStyle))),
                Expanded(
                    child: Center(child: Text("Produção", style: labelStyle))),
              ],
            ),
            Divider(),
            Expanded(
              child: Container(
                child: StreamBuilder<List<Lote>>(
                  stream: searchProvider.listBloc.list,
                  builder: (context, snapshot) {
                    Widget toShow;
                    if (snapshot.hasData &&
                        snapshot.data != null &&
                        snapshot.data.length > 0) {
                      toShow = Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: SmartRefresher(
                          controller: _refreshController,
                          footer: ClassicFooter(),
                          header: ClassicFooter(),
                          enablePullUp: true,
                          enablePullDown: false,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: _getWidgets(snapshot.data),
                          ),
                          onLoading: () async {
                            setState(() {
                              _loading = true;
                            });

                            await searchProvider.listBloc
                                .fetchList(query: searchProvider.searchQuery);

                            setState(() {
                              _loading = false;
                            });
                            _refreshController.loadComplete();
                          },
                        ),
                      );
                    } else {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        toShow = Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        toShow = SizedBox(
                          height: 0,
                        );
                      }
                    }
                    return toShow;
                  },
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
