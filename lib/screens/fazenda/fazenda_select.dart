import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/repositories/fazenda_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/theme/colors.dart' as colors;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:provider/provider.dart';

class FazendaSelect extends StatefulWidget {
  @override
  _FazendaSelectState createState() => _FazendaSelectState();
}

class _FazendaSelectState extends State<FazendaSelect> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _typeAheadController = TextEditingController();
  Fazenda _selectedFazenda;
  bool isLoading = false;
  FazendaRepository fazendaRepository = new FazendaRepository();

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var navegacaoProvider =
          Provider.of<NavegacaoHomeProvider>(context, listen: false);
      authProvider.bloc.refreshToken.listen((token) {
        if ((token?.status) == Status.ERROR &&
            navegacaoProvider.index == INDEX_FAZENDA) {
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      });
      authProvider.bloc.fazenda.listen((fazenda) {
        if (fazenda?.status == Status.COMPLETED &&
            navegacaoProvider.index == INDEX_FAZENDA) {
          navegacaoProvider.setNavegacao(INDEX_HOME);
        } else if (fazenda?.status == Status.ERROR &&
            (fazenda?.message ?? "") != "") {
          this._scaffoldKey.currentState?.showSnackBar(SnackBar(
                content: Text(fazenda.message),
                behavior: SnackBarBehavior.floating,
                duration: Duration(seconds: 4),
              ));
        }
      });
    });
    super.initState();
  }

  Future<List<Fazenda>> _getFazendas(String query) async {
    try {
      var fazendas = await fazendaRepository.getFazendas();
      return fazendas;
    } catch (e) {
      return <Fazenda>[];
    }
  }

  _getAccessToken() async {
    if (isLoading) return;
    setState(() {
      isLoading = true;
    });

    if (_selectedFazenda != null) {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      await authProvider.bloc.fetchFazendaToken(this._selectedFazenda);
    }

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[400],
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: colors.PRIMARY_GRADIENT_VERTICAL,
            ),
          ),
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "ACESSAR FAZENDA",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: Colors.white,
                    fontSize: 18,
                    letterSpacing: 0.5,
                  ),
                ),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Card(
                    color: colors.PRIMARY_SWATCH[50],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Form(
                          key: this._formKey,
                          child: Padding(
                            padding: EdgeInsets.all(32.0),
                            child: Column(
                              children: <Widget>[
                                TypeAheadFormField<Fazenda>(
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                          controller: this._typeAheadController,
                                          decoration: InputDecoration(
                                              labelText: 'Fazenda')),
                                  suggestionsCallback: _getFazendas,
                                  itemBuilder: (context, suggestion) {
                                    return ListTile(
                                      title: Text(suggestion.name),
                                    );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    this._typeAheadController.text =
                                        suggestion.name;
                                    this._selectedFazenda = suggestion;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Por favor, selecione um fazenda';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) => _getAccessToken(),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: SizedBox(
                            height: 42,
                            width: 230,
                            child: PillGradientButton(
                              "SELECIONAR",
                              () {
                                if (this._formKey.currentState.validate()) {
                                  this._formKey.currentState.save();
                                } else {
                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          'Uma fazenda precisa ser selecionada para prosseguir, caso nenhuma fazenda esteja aparecendo comunique o supervisor da sua Fazenda'),
                                      duration: Duration(seconds: 4),
                                    ),
                                  );

                                  this._scaffoldKey.currentState?.showSnackBar(
                                        SnackBar(
                                          content: Text(
                                              'Uma fazenda precisa ser selecionada para prosseguir, caso nenhuma fazenda esteja aparecendo comunique o supervisor da sua Fazenda'),
                                          duration: Duration(seconds: 4),
                                          behavior: SnackBarBehavior.floating,
                                        ),
                                      );
                                }
                              },
                              loading: isLoading,
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Container(
                    height: 42,
                    width: 230,
                    child: PillOutlineButton(
                      "TROCAR CONTA",
                      () {
                        var authProvider =
                            Provider.of<AuthProvider>(context, listen: false);
                        authProvider.bloc.logOut();
                      },
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
