import 'package:app_fazenda/common/common_bluetooth_conect.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/config.provider.dart' as config;
import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/screens/busca/busca_screen.dart';
import 'package:app_fazenda/screens/cadastro/cadastro_screen.dart';
import 'package:app_fazenda/screens/classificacao/classificacao_screen.dart';
import 'package:app_fazenda/screens/pesagem/pesagem_screen.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_fazenda/utils/theme/theme.dart' as themes;
import 'package:app_fazenda/utils/helpers/assets.dart' as assets;
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentPageIndex;

  List<Page> pages = [
    Page(
      title: "Dashboard",
      content: CommingSon(),
      icon: Icons.dashboard,
      menuLabel: "Dashboard",
    ),
    Page(
      title: "Cadastro",
      content: CadastroScreen(),
      icon: Icons.add,
      menuLabel: "Cadastrar",
    ),
    Page(
      title: "Busca",
      content: BuscaScreen(),
      icon: Icons.search,
      menuLabel: "Buscar",
    ),
    Page(
      title: "Pesagem",
      content: PesagemScreen(),
      icon: AssetImage(assets.ICON_PESAGEM),
      menuLabel: "Pesagem",
    ),
    Page(
      title: "Classificação",
      content: ClassificacaoScreen(),
      icon: Icons.format_line_spacing,
      menuLabel: "Classificação",
    )
  ];

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _currentPageIndex = 1;
    // Espera por erros quanto ao refresh token, caso ocorra redirecione para a tela de login
    Future.delayed(Duration.zero, () {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var navegacaoProvider =
          Provider.of<NavegacaoHomeProvider>(context, listen: false);
      authProvider.bloc.refreshToken.listen((token) {
        if ((token?.status) == Status.ERROR &&
            navegacaoProvider.index == INDEX_HOME) {
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      });
      authProvider.bloc.fazenda.listen((fazenda) {
        if ((fazenda?.status) == Status.ERROR &&
            navegacaoProvider.index == INDEX_HOME) {
          navegacaoProvider.setNavegacao(INDEX_FAZENDA);
        }
      });
    });

    super.initState();
  }

  _setPageIndex(int value) {
    setState(() {
      _currentPageIndex = value;
    });
  }

  List<PopupMenuItem<Function>> _getPopUpMenuOptions(BuildContext context) {
    return <PopupMenuItem<Function>>[
      PopupMenuItem(
        child: Text("Deslogar"),
        value: () {
          var provider = Provider.of<AuthProvider>(context, listen: false);
          provider.bloc.logOut();
        },
      ),
      PopupMenuItem(
        child: Text("Trocar Fazenda"),
        value: () {
          var provider = Provider.of<AuthProvider>(context, listen: false);
          provider.bloc.trocarFazenda();
        },
      ),
      PopupMenuItem(
        child: Text("Dark Mode"),
        value: () {
          var provider =
              Provider.of<config.ConfigProvider>(context, listen: false);
          provider.setThemeMode(config.ThemeMode.DARK);
        },
      ),
      PopupMenuItem(
        child: Text("Light Mode"),
        value: () {
          var provider =
              Provider.of<config.ConfigProvider>(context, listen: false);
          provider.setThemeMode(config.ThemeMode.LIGHT);
        },
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));

    return Consumer<config.ConfigProvider>(
      builder: (context, provider, child) {
        if (provider.themeMode != null) {
          ThemeData theme = provider.themeMode == config.ThemeMode.DARK
              ? themes.mainThemeDark
              : themes.mainTheme;
          return Theme(
            data: theme,
            child: Scaffold(
                key: _scaffoldKey,
                appBar: AppBar(
                  title: Text(pages[_currentPageIndex].title),
                  leading: IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: () => {},
                  ),
                  actions: [
                    PopupMenuButton<Function>(
                      icon: Icon(Icons.more_vert),
                      itemBuilder: _getPopUpMenuOptions,
                      onSelected: (func) => func(),
                    )
                  ],
                ),
                body: pages[_currentPageIndex].content,
                bottomNavigationBar: new Theme(
                  data: provider.themeMode == config.ThemeMode.DARK
                      ? themes.bottomNavigationBarThemeDark
                      : themes.bottomNavigationBarTheme,
                  child: BottomNavigationBar(
                    items: pages
                        .map<BottomNavigationBarItem>(
                            (e) => BottomNavigationBarItem(
                                  icon: e.icon.runtimeType == IconData
                                      ? Icon(e.icon)
                                      : ImageIcon(e.icon),
                                  title: Text(e.title),
                                ))
                        .toList(),
                    currentIndex: _currentPageIndex,
                    onTap: _setPageIndex,
                  ),
                )),
          );
        } else {
          return Container(
            color: PRIMARY_SWATCH,
          );
        }
      },
    );
  }
}

class Page {
  String title;
  Widget content;
  dynamic icon;
  String menuLabel;

  Page({this.title, this.content, this.menuLabel, this.icon});
}

class CommingSon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "EM BREVE!",
        style: TextStyle(
            color: Colors.grey[600], fontSize: 30, fontWeight: FontWeight.w600),
      ),
    );
  }
}
