import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/repositories/animal_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:provider/provider.dart';

// {
//     "nome": "Mimosa",
//     "data_nascimento": "2018-09-22T12:42:31Z",
//     "sexo": "F",
//     "producao": true,
//     "fazenda_id": 1,
//     "numero_chip": "900 1122334455",
//     "numero_brinco": 23,
//     "proprio": true,
//     "status": "ativo"
// }

class FormAnimal extends StatefulWidget {
  @override
  _FormAnimalState createState() => _FormAnimalState();
}

class _FormAnimalState extends State<FormAnimal> {
  final formKey = new GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _numeroChipController = TextEditingController();
  final TextEditingController _numeroBrincoController = TextEditingController();
  final format = DateFormat("dd/MM/yyyy");
  final animalRepository = AnimalRepository();

  DateTime _dataNascimento;
  String _sexo;
  String _producao;
  String _status = "ativo";

  bool showRequired = true;
  bool _isLoading = false;

  void onSubmit() async {
    setState(() {
      _isLoading = true;
    });
    String error;
    if (formKey.currentState.validate()) {
      if (_nameController.text.length == 0 ||
          _numeroBrincoController.text.length == 0 ||
          _numeroChipController.text.length == 0 ||
          (_sexo ?? "") == "") {
        error = "Preencha os campos obrigatórios para o cadastro.";
      } else {
        AuthProvider authProvider =
            Provider.of<AuthProvider>(context, listen: false);
        try {
          var animal = await animalRepository.createAnimal(
            fazendaID: authProvider.bloc.fazendaObject?.id,
            token: authProvider.bloc.accessTokenObject,
            nome: _nameController.text,
            numeroChip: _numeroChipController.text,
            numeroBrinco: _numeroBrincoController.text,
            sexo: _sexo,
            dataNascimento: _dataNascimento,
            producao: _producao != null ? (_producao == "S") : null,
            status: _status,
          );

          if (animal != null) {
            print("Animal Criado!");
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text(
                "Animal cadastrado com sucesso!",
                style: TextStyle(color: Colors.greenAccent),
              ),
              behavior: SnackBarBehavior.floating,
            ));
          }
        } catch (e) {
          print(e);
          if (e.runtimeType == ServiceHandableException) {
            ServiceHandableException exception =
                (e as ServiceHandableException);
            error = exception.message;
            if (exception.message.contains("TOKEN_EXPIRED")) {
              error =
                  "Token expirado faça login.. por favor faça login novamente.";
              authProvider.bloc.changeAccessToken(ApiResponse.error(""));
            }
          } else {
            error =
                "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
          }
        }
      }
    } else {
      error = "Alguns campos contém erros.";
    }
    if (error != null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> optionalFields = [
      DateTimeField(
        initialValue: _dataNascimento,
        format: format,
        decoration: InputDecoration(
          labelText: "Data de Nascimento:",
        ),
        onShowPicker: (context, currentValue) async {
          var date = await showDatePicker(
            context: context,
            firstDate: DateTime(1900),
            initialDate: currentValue ?? DateTime.now(),
            lastDate: DateTime(2100),
          );
          setState(() {
            _dataNascimento = date;
          });
          return date;
        },
      ),
      SizedBox(height: 10),
      DropDownFormField(
        titleText: 'Produção:',
        hintText: 'O animal é de produção?',
        value: _producao,
        onSaved: (value) {
          setState(() {
            _producao = value;
          });
        },
        onChanged: (value) {
          setState(() {
            _producao = value;
          });
        },
        dataSource: [
          {
            "display": "Sim",
            "value": "S",
          },
          {
            "display": "Não",
            "value": "N",
          },
        ],
        textField: 'display',
        valueField: 'value',
      ),
      SizedBox(height: 10),
      DropDownFormField(
        titleText: 'Status:',
        hintText: 'Informe o status do animal',
        value: _status,
        onSaved: (value) {
          setState(() {
            _status = value;
          });
        },
        onChanged: (value) {
          setState(() {
            _status = value;
          });
        },
        dataSource: [
          {
            "display": "Ativo",
            "value": "ativo",
          },
          {
            "display": "Baixado",
            "value": "baixado",
          },
        ],
        textField: 'display',
        valueField: 'value',
      ),
    ];
    List<Widget> requiredFields = [
      TextFormField(
        controller: _nameController,
        decoration: InputDecoration(
          labelText: "Nome:",
        ),
        keyboardType: TextInputType.text,
      ),
      SizedBox(height: 10),
      TextFormField(
        controller: _numeroChipController,
        decoration: InputDecoration(
          labelText: "Numero do Chip:",
        ),
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
      ),
      SizedBox(height: 10),
      TextFormField(
        controller: _numeroBrincoController,
        decoration: InputDecoration(
          labelText: "Numero do Brinco:",
        ),
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
      ),
      SizedBox(height: 10),
      DropDownFormField(
        titleText: 'Sexo:',
        hintText: 'Selecione o sexo',
        value: _sexo,
        onSaved: (value) {
          setState(() {
            _sexo = value;
          });
        },
        onChanged: (value) {
          setState(() {
            _sexo = value;
          });
        },
        dataSource: [
          {
            "display": "Feminino",
            "value": "F",
          },
          {
            "display": "Masculino",
            "value": "M",
          },
        ],
        textField: 'display',
        valueField: 'value',
      ),
    ];

    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Form(
                key: formKey,
                child: ListView(
                  children: [
                    if (showRequired) ...requiredFields,
                    if (!showRequired) ...optionalFields,
                    SizedBox(height: 10),
                    SizedBox(
                      height: 30,
                      width: 200,
                      child: PillOutlineButton(
                        showRequired
                            ? "Campos Opcionais"
                            : "Campos Obrigatórios",
                        () {
                          setState(() {
                            showRequired = !showRequired;
                          });
                        },
                        color: Colors.grey[500],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: SizedBox(
                height: 42,
                width: 230,
                child: PillGradientButton(
                  "CADASTRAR",
                  onSubmit,
                  loading: _isLoading,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
