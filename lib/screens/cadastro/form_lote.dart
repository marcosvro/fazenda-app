import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/repositories/lote_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormLote extends StatefulWidget {
  @override
  _FormLoteState createState() => _FormLoteState();
}

class _FormLoteState extends State<FormLote> {
  final formKey = new GlobalKey<FormState>();
  final loteRepository = LoteRepository();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();

  bool _isLoading = false;
  String _producao = "S";

  void onSubmit() async {
    setState(() {
      _isLoading = true;
    });
    String error;
    if (formKey.currentState.validate()) {
      if (_nameController.text.length == 0) {
        error = "Preencha os campos obrigatórios para o cadastro.";
      } else {
        AuthProvider authProvider =
            Provider.of<AuthProvider>(context, listen: false);
        try {
          var lote = await loteRepository.createLote(
            fazendaID: authProvider.bloc.fazendaObject?.id,
            token: authProvider.bloc.accessTokenObject,
            nome: _nameController.text,
            descricao: _descriptionController.text.length != 0
                ? _descriptionController.text
                : null,
            producao: _producao != null ? (_producao == "S") : null,
          );

          if (lote != null) {
            print("Lote Criado!");
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text(
                "Lote cadastrado com sucesso!",
                style: TextStyle(color: Colors.greenAccent),
              ),
              behavior: SnackBarBehavior.floating,
            ));
          }
        } catch (e) {
          print(e);
          if (e.runtimeType == ServiceHandableException) {
            ServiceHandableException exception =
                (e as ServiceHandableException);
            error = exception.message;
            if (exception.message.contains("TOKEN_EXPIRED")) {
              error =
                  "Token expirado faça login.. por favor faça login novamente.";
              authProvider.bloc.changeAccessToken(ApiResponse.error(""));
            }
          } else {
            error =
                "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
          }
        }
      }
    } else {
      error = "Alguns campos contém erros.";
    }
    if (error != null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Form(
              key: formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                      labelText: "Nome:",
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    controller: _descriptionController,
                    decoration: InputDecoration(
                      labelText: "Descrição:",
                    ),
                    autocorrect: true,
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(height: 10),
                  DropDownFormField(
                    titleText: 'Produção:',
                    hintText: 'O animal é de produção?',
                    value: _producao,
                    onSaved: (value) {
                      setState(() {
                        _producao = value;
                      });
                    },
                    onChanged: (value) {
                      setState(() {
                        _producao = value;
                      });
                    },
                    dataSource: [
                      {
                        "display": "Sim",
                        "value": "S",
                      },
                      {
                        "display": "Não",
                        "value": "N",
                      },
                    ],
                    textField: 'display',
                    valueField: 'value',
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: SizedBox(
                height: 42,
                width: 230,
                child: PillGradientButton(
                  "CADASTRAR",
                  onSubmit,
                  loading: _isLoading,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
