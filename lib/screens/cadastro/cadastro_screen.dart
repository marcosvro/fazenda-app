import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/screens/cadastro/form_animal.dart';
import 'package:app_fazenda/screens/cadastro/form_lote.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// WIDGET CHECK
class CadastroScreen extends StatefulWidget {
  @override
  _CadastroScreenState createState() => _CadastroScreenState();
}

class _CadastroScreenState extends State<CadastroScreen> {
  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);

    return StreamBuilder<ApiResponse<Fazenda>>(
      stream: authProvider.bloc.fazenda,
      builder: (context, snapshot) {
        var loadingContent = Center(
          child: CircularProgressIndicator(),
        );

        if (snapshot.hasData && snapshot.data.status == Status.COMPLETED) {
          Fazenda fazenda = snapshot.data.data;
          return CadastrarNaFazenda(
            fazenda: fazenda,
          );
        } else {
          return loadingContent;
        }
      },
    );
  }
}

// WIDGET CONTENT

class CadastrarNaFazenda extends StatefulWidget {
  final Fazenda fazenda;

  CadastrarNaFazenda({@required this.fazenda});

  @override
  _CadastrarNaFazendaState createState() => _CadastrarNaFazendaState();
}

class _CadastrarNaFazendaState extends State<CadastrarNaFazenda> {
  String _activeForm;

  Map<String, Widget> forms = {
    'animal_form': FormAnimal(),
    'lote_form': FormLote(),
  };

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: DropDownFormField(
            titleText: 'Cadastro de:',
            hintText: 'Oque quer cadastrar?',
            value: _activeForm,
            onSaved: (value) {
              setState(() {
                _activeForm = value;
              });
            },
            onChanged: (value) {
              setState(() {
                _activeForm = value;
              });
            },
            dataSource: [
              {
                "display": "Animal",
                "value": "animal_form",
              },
              {
                "display": "Lote",
                "value": "lote_form",
              },
            ],
            textField: 'display',
            valueField: 'value',
          ),
        ),
        SizedBox(height: 10),
        if (_activeForm != null) forms[_activeForm],
      ],
    );
  }
}
