import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/screens/fazenda/fazenda_select.dart';
import 'package:app_fazenda/screens/home/home.dart';
import 'package:app_fazenda/screens/login/login.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ScreenRouter extends StatefulWidget {
  @override
  _ScreenRouterState createState() => _ScreenRouterState();
}

class _ScreenRouterState extends State<ScreenRouter> {
  @override
  Widget build(BuildContext context) {
    return Consumer<NavegacaoHomeProvider>(
      builder: (context, provider, child) {
        switch (provider.index) {
          case INDEX_HOME:
            return Home();
          case INDEX_LOGIN:
            return Login();
          case INDEX_FAZENDA:
            return FazendaSelect();
          default:
            return null;
        }
      },
    );
  }
}
