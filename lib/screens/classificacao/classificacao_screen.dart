// WIDGET CHECK
import 'package:app_fazenda/blocs/bloc_classificacao.dart';
import 'package:app_fazenda/models/animal.dart';
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/models/pesagem.dart';
import 'package:app_fazenda/providers/config.provider.dart' as config;
import 'package:app_fazenda/models/lote.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/screens/pesagem/pesagem_card_info.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ClassificacaoScreen extends StatefulWidget {
  @override
  _ClassificacaoScreenState createState() => _ClassificacaoScreenState();
}

class _ClassificacaoScreenState extends State<ClassificacaoScreen> {
  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);

    return StreamBuilder<ApiResponse<Fazenda>>(
      stream: authProvider.bloc.fazenda,
      builder: (context, snapshot) {
        var loadingContent = Center(
          child: CircularProgressIndicator(),
        );

        if (snapshot.hasData && snapshot.data.status == Status.COMPLETED) {
          Fazenda fazenda = snapshot.data.data;
          return ClassificacaoContent(
            fazenda: fazenda,
          );
        } else {
          return loadingContent;
        }
      },
    );
  }
}

// WIDGET CONTENT
class ClassificacaoContent extends StatefulWidget {
  final Fazenda fazenda;

  const ClassificacaoContent({Key key, @required this.fazenda})
      : super(key: key);

  @override
  _ClassificacaoContentState createState() => _ClassificacaoContentState();
}

class _ClassificacaoContentState extends State<ClassificacaoContent> {
  ClassificacaoBloc classificacaoBloc = ClassificacaoBloc();
  bool isLoadingLotes = false;
  bool isSwapingAnimal = false;
  List<Lote> loteList = [];
  Pesagem lastPesagem;

  @override
  void initState() {
    super.initState();
    fetchLoteList();
  }

  fetchLoteList() async {
    setState(() {
      isLoadingLotes = true;
    });
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    var err = await classificacaoBloc.fetchLotes(
        authProvider.bloc.accessTokenObject, authProvider.bloc.fazendaObject);

    setState(() {
      loteList = classificacaoBloc.lotes ?? [];
      lastPesagem = classificacaoBloc.lastPesagem;
      isLoadingLotes = false;
    });

    if (err != null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          err,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
  }

  changeAnimalLote(Animal animal, Lote newLote, int newLoteIndex) async {
    setState(() {
      isSwapingAnimal = true;
    });
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    var err = await classificacaoBloc.moveAnimalBetweenLotes(
        authProvider.bloc.accessTokenObject, animal, newLote, newLoteIndex);

    setState(() {
      loteList = classificacaoBloc.lotes ?? [];
      isSwapingAnimal = false;
    });

    if (err != null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          err,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<config.ConfigProvider>(context, listen: false);
    return Scaffold(
      body: isLoadingLotes
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      DragAndDropLists(
                        children: [
                          if (lastPesagem != null)
                            DragAndDropList(
                              header: Center(
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    "Última Pesagem Finalizada",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ),
                              canDrag: false,
                              contentsWhenEmpty: PesagemCardInfo(
                                  pesagem: lastPesagem, finishing: false),
                            ),
                          ...List.generate(
                              loteList.length, (index) => _buildList(index))
                        ],
                        onItemReorder: _onItemReorder,
                        onListReorder: _onListReorder,
                        // listGhost is mandatory when using expansion tiles to prevent multiple widgets using the same globalkey
                        listGhost: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 30.0),
                          child: Center(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 30.0, horizontal: 100.0),
                              decoration: BoxDecoration(
                                border: Border.all(),
                                borderRadius: BorderRadius.circular(7.0),
                              ),
                              child: Icon(Icons.add_box),
                            ),
                          ),
                        ),
                      ),
                      if (isSwapingAnimal)
                        Container(
                          color: provider.themeMode == config.ThemeMode.DARK
                              ? Color.fromARGB(25, 255, 255, 255)
                              : Color.fromARGB(100, 0, 0, 0),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }

  _buildList(int outerIndex) {
    var innerList = loteList[outerIndex];
    return DragAndDropListExpansion(
      canDrag: false,
      title: Text(innerList.id != null ? innerList.nome : "Animais sem lote"),
      subtitle: Text(innerList.descricao ?? ""),
      leading: Icon(Icons.reorder),
      contentsWhenEmpty: Text("Lote vazio"),
      children: List.generate(innerList.animais.length,
          (index) => _buildItem(innerList.animais[index])),
      listKey: ObjectKey(innerList),
    );
  }

  _buildItem(Animal animal) {
    return DragAndDropItem(
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(horizontal: 10),
        enabled: false,
        title: Card(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('#${animal.numeroBrinco}'),
                Text(animal.ultimaMedicao != null
                    ? '${animal.ultimaMedicao} L'
                    : ''),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _onItemReorder(
      int oldItemIndex, int oldListIndex, int newItemIndex, int newListIndex) {
    if (lastPesagem != null) {
      oldListIndex--;
      newListIndex--;
    }
    changeAnimalLote(loteList[oldListIndex].animais[oldItemIndex],
        loteList[newListIndex], newItemIndex);
  }

  _onListReorder(int oldListIndex, int newListIndex) {
    if (lastPesagem != null) {
      oldListIndex--;
      newListIndex--;
    }
    setState(() {
      var movedList = loteList.removeAt(oldListIndex);
      loteList.insert(newListIndex, movedList);
    });
  }
}
