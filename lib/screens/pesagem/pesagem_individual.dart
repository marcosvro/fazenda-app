import 'package:app_fazenda/blocs/bloc_pesagem.dart';
import 'package:app_fazenda/common/common_bluetooth_conect.dart';
import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/common/common_leitora_button.dart';
import 'package:app_fazenda/models/animal.dart';
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/models/pesagem.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/repositories/animal_repository.dart';
import 'package:app_fazenda/repositories/pesagem_repository.dart';
import 'package:app_fazenda/screens/pesagem/pesagem_card_info.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class PesagemIndividualContent extends StatefulWidget {
  final Pesagem pesagem;

  const PesagemIndividualContent({Key key, @required this.pesagem})
      : super(key: key);

  @override
  _PesagemIndividualContentState createState() =>
      _PesagemIndividualContentState();
}

class _PesagemIndividualContentState extends State<PesagemIndividualContent> {
  final TextEditingController _typeAheadController = TextEditingController();
  final TextEditingController _pesoController = TextEditingController();
  AnimalRepository animalRepository = new AnimalRepository();
  PesagemRepository pesagemRepository = new PesagemRepository();
  Animal _selectedAnimal;
  bool _loading = false;
  bool _finishing = false;

  void listPesagens() {
    PesagemBloc pesagemBloc = Provider.of<PesagemBloc>(context, listen: false);
    pesagemBloc.changePesagem(null);
  }

  Future<List<Animal>> _getAnimais(String query) async {
    try {
      AuthProvider authProvider =
          Provider.of<AuthProvider>(context, listen: false);
      var animais = await animalRepository.getAnimais(
        fazendaID: authProvider.bloc.fazendaObject?.id,
        token: authProvider.bloc.accessTokenObject,
        queryKey: "numero_brinco",
        queryValue: query,
      );
      print(animais);
      return animais;
    } catch (e) {
      print(e);
      return <Animal>[];
    }
  }

  _submitPesagemIndividual() async {
    if (_loading || _finishing) return;
    setState(() {
      _loading = true;
    });
    try {
      AuthProvider authProvider =
          Provider.of<AuthProvider>(context, listen: false);
      await this.pesagemRepository.createPesagemIndividual(
            fazendaID: authProvider.bloc.fazendaObject?.id,
            pesagemID: widget.pesagem?.id,
            vacaID: _selectedAnimal?.id,
            peso: double.parse(_pesoController.text.replaceAll(",", ".")),
            token: authProvider.bloc.accessTokenObject,
          );
      PesagemBloc pesagemBloc =
          Provider.of<PesagemBloc>(context, listen: false);
      widget.pesagem.pesoTotal = widget.pesagem.pesoTotal == null
          ? double.parse(_pesoController.text.replaceAll(",", "."))
          : widget.pesagem.pesoTotal +
              double.parse(_pesoController.text.replaceAll(",", "."));
      widget.pesagem.qtdPesagens = (widget.pesagem.qtdPesagens ?? 0) + 1;
      widget.pesagem.updatedAt = new DateTime.now();
      pesagemBloc.changePesagem(ApiResponse.completed(widget.pesagem));
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          "Pesagem individual cadastrada com sucesso!",
          style: TextStyle(color: Colors.greenAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    } catch (e) {
      print(e);
      AuthProvider authProvider =
          Provider.of<AuthProvider>(context, listen: false);
      String error;
      if (e.runtimeType == ServiceHandableException) {
        ServiceHandableException exception = (e as ServiceHandableException);
        error = exception.message;
        if (exception.message.contains("TOKEN_EXPIRED")) {
          error = "Token expirado faça login.. por favor faça login novamente.";
          authProvider.bloc.changeAccessToken(ApiResponse.error(""));
          var navegacaoProvider =
              Provider.of<NavegacaoHomeProvider>(context, listen: false);
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      } else {
        error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
      }
      print(error);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
    setState(() {
      _loading = false;
    });
  }

  _finalizarPesagem() async {
    if (_loading || _finishing) return;
    setState(() {
      _finishing = true;
    });
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    try {
      await this.pesagemRepository.finalizaPesagem(
            pesagemID: widget.pesagem?.id,
            token: authProvider.bloc.accessTokenObject,
          );
      PesagemBloc pesagemBloc =
          Provider.of<PesagemBloc>(context, listen: false);
      widget.pesagem.finalizada = true;
      widget.pesagem.updatedAt = new DateTime.now();
      print(widget.pesagem.createdAt.toIso8601String());
      print(widget.pesagem.updatedAt.toIso8601String());
      pesagemBloc.changePesagem(ApiResponse.completed(widget.pesagem));
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          "Pesagem finalizada com sucesso!",
          style: TextStyle(color: Colors.greenAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    } catch (e) {
      print(e);
      String error;
      if (e.runtimeType == ServiceHandableException) {
        ServiceHandableException exception = (e as ServiceHandableException);
        error = exception.message;
        if (exception.message.contains("TOKEN_EXPIRED")) {
          error = "Token expirado faça login.. por favor faça login novamente.";
          authProvider.bloc.changeAccessToken(ApiResponse.error(""));
          var navegacaoProvider =
              Provider.of<NavegacaoHomeProvider>(context, listen: false);
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      } else {
        error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
      }
      print(error);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
    setState(() {
      _finishing = false;
    });
  }

  _novaPesagem() async {
    if (_loading || _finishing) return;
    setState(() {
      _loading = true;
    });
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    try {
      Pesagem pesagem = await this.pesagemRepository.createPesagem(
            token: authProvider.bloc.accessTokenObject,
            fazendaID: authProvider.bloc.fazendaObject?.id,
          );

      if (pesagem != null) {
        PesagemBloc pesagemBloc =
            Provider.of<PesagemBloc>(context, listen: false);
        pesagemBloc.changePesagem(ApiResponse.completed(pesagem));
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
            "Pesagem criada com sucesso!",
            style: TextStyle(color: Colors.greenAccent),
          ),
          behavior: SnackBarBehavior.floating,
        ));
      }
    } catch (e) {
      print(e);
      String error;
      if (e.runtimeType == ServiceHandableException) {
        ServiceHandableException exception = (e as ServiceHandableException);
        error = exception.message;
        if (exception.message.contains("TOKEN_EXPIRED")) {
          error = "Token expirado faça login.. por favor faça login novamente.";
          authProvider.bloc.changeAccessToken(ApiResponse.error(""));
          var navegacaoProvider =
              Provider.of<NavegacaoHomeProvider>(context, listen: false);
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      } else {
        error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
      }
      print(error);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          PesagemCardInfo(
            finishing: _finishing,
            pesagem: widget.pesagem,
            back: listPesagens,
            finalizarPesagem: _finalizarPesagem,
          ),
          SizedBox(height: 10),
          if (!(widget.pesagem?.finalizada ?? false))
            Container(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Cadastrar Medição",
                      style: TextStyle(fontSize: 16),
                    ),
                    SizedBox(height: 20),
                    Stack(
                      fit: StackFit.loose,
                      children: [
                        TypeAheadFormField<Animal>(
                          textFieldConfiguration: TextFieldConfiguration(
                              controller: this._typeAheadController,
                              decoration: InputDecoration(labelText: 'Animal')),
                          suggestionsCallback: _getAnimais,
                          itemBuilder: (context, suggestion) {
                            return ListTile(
                              title: Text('#${suggestion?.numeroBrinco}'),
                            );
                          },
                          transitionBuilder:
                              (context, suggestionsBox, controller) {
                            return suggestionsBox;
                          },
                          onSuggestionSelected: (suggestion) {
                            this._typeAheadController.text =
                                '${suggestion.numeroBrinco}';
                            this._selectedAnimal = suggestion;
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Por favor, selecione um animal';
                            }
                            return null;
                          },
                          onSaved: (value) => _submitPesagemIndividual(),
                        ),
                        Positioned(
                          right: 10,
                          child: Container(
                            height: 67,
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                LeitoraButtton(onAnimalReceived: (animal) {
                                  this._typeAheadController.text =
                                      '${animal.numeroBrinco}';
                                  this._selectedAnimal = animal;
                                }),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 15),
                    TextFormField(
                      controller: _pesoController,
                      decoration: InputDecoration(
                        labelText: "Medição",
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                          decimal: true, signed: true),
                      inputFormatters: [],
                    ),
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: SizedBox(
                        height: 42,
                        width: 230,
                        child: PillGradientButton(
                          "CADASTRAR",
                          _submitPesagemIndividual,
                          loading: _loading,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          else
            Container(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      "Pesagem finalizada, para carastrar novas medições crie outra pesagem.",
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 15),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: SizedBox(
                        height: 42,
                        width: 230,
                        child: PillOutlineButton(
                          "NOVA PESAGEM",
                          _novaPesagem,
                          loading: _loading,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
