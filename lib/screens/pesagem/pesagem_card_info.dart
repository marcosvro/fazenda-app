import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/models/pesagem.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PesagemCardInfo extends StatelessWidget {
  const PesagemCardInfo({
    Key key,
    @required this.pesagem,
    bool finishing,
    this.back,
    this.finalizarPesagem,
  })  : _finishing = finishing,
        super(key: key);

  final Pesagem pesagem;
  final Function back;
  final Function finalizarPesagem;
  final bool _finishing;

  List<Widget> getPesagemField(String label, String value) {
    return <Widget>[
      Divider(height: 3),
      Container(
        height: 40,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text(label), Text(value)],
          ),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (finalizarPesagem != null || back != null)
            Container(
              height: 40,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if (back != null)
                      IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: back,
                      ),
                    Expanded(child: Center(child: Text("Pesagem"))),
                    if (!(pesagem?.finalizada ?? false) &&
                        finalizarPesagem != null)
                      Container(
                        height: 20,
                        child: PillOutlineButton(
                          "Finalizar",
                          finalizarPesagem,
                          loading: _finishing,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ...getPesagemField("ID", pesagem.id.toString()),
          ...getPesagemField(
              "Status", (pesagem.finalizada ?? false) ? "Finalizada" : "Ativa"),
          ...getPesagemField(
              "Data de Criação",
              pesagem.createdAt != null
                  ? DateFormat("dd/MM/yyyy hh:mm").format(pesagem.createdAt)
                  : " - "),
          ...getPesagemField(
              "Última Atualização",
              pesagem.createdAt != null
                  ? DateFormat("dd/MM/yyyy hh:mm").format(pesagem.updatedAt)
                  : " - "),
          ...getPesagemField(
              "Medição Total", "${(pesagem.pesoTotal ?? 0.0).toString()} Lts"),
          ...getPesagemField(
              "Número de Medições", "${(pesagem.qtdPesagens ?? 0).toString()}"),
          SizedBox(height: 3),
        ],
      ),
    );
  }
}
