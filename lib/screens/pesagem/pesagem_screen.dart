import 'package:app_fazenda/blocs/bloc_pesagem.dart';
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/models/pesagem.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/screens/pesagem/pesagem_individual.dart';
import 'package:app_fazenda/screens/pesagem/pesagem_list.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// WIDGET CHECK
class PesagemScreen extends StatefulWidget {
  @override
  _PesagemScreenState createState() => _PesagemScreenState();
}

class _PesagemScreenState extends State<PesagemScreen> {
  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(context, listen: false);

    return StreamBuilder<ApiResponse<Fazenda>>(
      stream: authProvider.bloc.fazenda,
      builder: (context, snapshot) {
        var loadingContent = Center(
          child: CircularProgressIndicator(),
        );

        if (snapshot.hasData && snapshot.data.status == Status.COMPLETED) {
          Fazenda fazenda = snapshot.data.data;
          return PesagemContent(
            fazenda: fazenda,
          );
        } else {
          return loadingContent;
        }
      },
    );
  }
}

// CONTENT WIDGET
class PesagemContent extends StatefulWidget {
  final Fazenda fazenda;

  const PesagemContent({Key key, @required this.fazenda}) : super(key: key);

  @override
  _PesagemContentState createState() => _PesagemContentState();
}

class _PesagemContentState extends State<PesagemContent> {
  PesagemBloc pesagemBloc;

  @override
  void initState() {
    this.pesagemBloc = new PesagemBloc();
    super.initState();
  }

  @override
  void dispose() {
    this.pesagemBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Provider.value(
      value: this.pesagemBloc,
      child: StreamBuilder<ApiResponse<Pesagem>>(
        stream: this.pesagemBloc.pesagem,
        builder: (context, snapshot) {
          var loadingContent = Center(
            child: CircularProgressIndicator(),
          );

          if (snapshot.hasData && snapshot.data.status == Status.COMPLETED) {
            Pesagem pesagem = snapshot.data.data;
            return PesagemIndividualContent(pesagem: pesagem);
          } else if (snapshot.hasData &&
              snapshot.data.status == Status.LOADING) {
            return loadingContent;
          } else {
            return PesagemList();
          }
        },
      ),
    );
  }
}
