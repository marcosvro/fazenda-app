import 'package:app_fazenda/blocs/bloc_paginated_list.dart';
import 'package:app_fazenda/blocs/bloc_pesagem.dart';
import 'package:app_fazenda/blocs/bloc_search.dart';
import 'package:app_fazenda/models/pesagem.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/providers/search.provider.dart';
import 'package:app_fazenda/repositories/pesagem_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:app_fazenda/utils/helpers/scroll_behaviours.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:intl/intl.dart';

class PesagemList extends StatefulWidget {
  @override
  _PesagemListState createState() => _PesagemListState();
}

class _PesagemListState extends State<PesagemList> {
  PesagemRepository pesagemRepository = PesagemRepository();
  SearchProvider _provider = SearchProvider();

  final RefreshController _refreshController = RefreshController(
    initialRefresh: false,
  );

  bool _loading = false;
  bool _creatingPesagem = false;

  @override
  void initState() {
    _provider.setListBloc<Pesagem>(this.getPesagemList);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _refreshController.dispose();
    try {
      _provider.disposeListBloc();
    } catch (e) {}
  }

  void selectPesagem(Pesagem pesagem) {
    PesagemBloc pesagemBloc = Provider.of<PesagemBloc>(context, listen: false);
    pesagemBloc.changePesagem(ApiResponse.completed(pesagem));
  }

  Future<List<Pesagem>> getPesagemList(
      PageConfig pageConfig, SearchQuery searchQuery) async {
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    try {
      var pesagens = await pesagemRepository.getPesagens(
        token: authProvider.bloc.accessTokenObject,
        page: pageConfig?.page,
        pageSize: pageConfig?.size,
      );

      return pesagens;
    } catch (e) {
      print(e);
      String error;
      if (e.runtimeType == ServiceHandableException) {
        ServiceHandableException exception = (e as ServiceHandableException);
        error = exception.message;
        if (exception.message.contains("TOKEN_EXPIRED")) {
          error = "Token expirado faça login.. por favor faça login novamente.";
          authProvider.bloc.changeAccessToken(ApiResponse.error(""));
          var navegacaoProvider =
              Provider.of<NavegacaoHomeProvider>(context, listen: false);
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      } else {
        error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
      }
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
      return null;
    }
  }

  List<Widget> _getWidgets(List<Pesagem> list) {
    var labelStyle = TextStyle(fontSize: 12);
    return list
        .map<Widget>((e) => Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 40,
                  child: FlatButton(
                    onPressed: () => selectPesagem(e),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: Center(
                                child: Text(e?.id.toString() ?? "-",
                                    style: labelStyle))),
                        Expanded(
                            child: Center(
                                child: Text(
                                    (e.finalizada ?? false)
                                        ? "Finalizada"
                                        : "Ativa",
                                    style: labelStyle))),
                        Expanded(
                            child: Center(
                                child: Text(
                                    e.createdAt != null
                                        ? DateFormat("dd/MM/yyyy")
                                            .format(e.createdAt)
                                        : "-",
                                    style: labelStyle))),
                      ],
                    ),
                  ),
                ),
                Divider(),
              ],
            ))
        .toList();
  }

  _novaPesagem() async {
    if (_creatingPesagem) return;
    setState(() {
      _creatingPesagem = true;
    });
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    try {
      Pesagem pesagem = await this.pesagemRepository.createPesagem(
            token: authProvider.bloc.accessTokenObject,
            fazendaID: authProvider.bloc.fazendaObject?.id,
          );

      if (pesagem != null) {
        PesagemBloc pesagemBloc =
            Provider.of<PesagemBloc>(context, listen: false);
        pesagemBloc.changePesagem(ApiResponse.completed(pesagem));
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
            "Pesagem criada com sucesso!",
            style: TextStyle(color: Colors.greenAccent),
          ),
          behavior: SnackBarBehavior.floating,
        ));
      }
    } catch (e) {
      print(e);
      String error;
      if (e.runtimeType == ServiceHandableException) {
        ServiceHandableException exception = (e as ServiceHandableException);
        error = exception.message;
        if (exception.message.contains("TOKEN_EXPIRED")) {
          error = "Token expirado faça login.. por favor faça login novamente.";
          authProvider.bloc.changeAccessToken(ApiResponse.error(""));
          var navegacaoProvider =
              Provider.of<NavegacaoHomeProvider>(context, listen: false);
          navegacaoProvider.setNavegacao(INDEX_LOGIN);
        }
      } else {
        error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
      }
      print(error);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
    setState(() {
      _creatingPesagem = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    const labelStyle = TextStyle(fontSize: 12, color: PRIMARY_SWATCH);
    return Scaffold(
      body: ChangeNotifierProvider.value(
        value: _provider,
        child: Consumer<SearchProvider>(
          builder: (context, provider, child) {
            if (provider.listBloc != null &&
                provider.listBloc.list is Stream<List<Pesagem>>) {
              return ScrollConfiguration(
                behavior: CustomScrollBehavior(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      SizedBox(height: 30),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              child:
                                  Center(child: Text("ID", style: labelStyle))),
                          Expanded(
                              child: Center(
                                  child: Text("Status", style: labelStyle))),
                          Expanded(
                              child: Center(
                                  child: Text("Data de criação",
                                      style: labelStyle))),
                        ],
                      ),
                      Divider(),
                      Expanded(
                        child: Container(
                          child: StreamBuilder<List<Pesagem>>(
                            stream: provider.listBloc.list,
                            builder: (context, snapshot) {
                              Widget toShow;
                              if (snapshot.hasData &&
                                  snapshot.data != null &&
                                  snapshot.data.length > 0) {
                                toShow = Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: SmartRefresher(
                                    controller: _refreshController,
                                    footer: ClassicFooter(
                                      outerBuilder: (child) => Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 15, horizontal: 20),
                                        child: child,
                                      ),
                                      height: 100,
                                      failedText:
                                          'Não foi possível carregar mais pesagens...',
                                      idleText:
                                          'Arraste para carregar mais pesagens',
                                      loadingText: 'Carregando mais pesagens',
                                      noDataText:
                                          'Você chegou ao fim da lista de pesagens!',
                                      canLoadingText:
                                          "Arraste para carregar mais pesagens",
                                    ),
                                    enablePullUp: true,
                                    enablePullDown: false,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: _getWidgets(snapshot.data),
                                    ),
                                    onLoading: () async {
                                      setState(() {
                                        _loading = true;
                                      });

                                      await provider.listBloc.fetchList(
                                          controller: _refreshController,
                                          query: provider.searchQuery);

                                      setState(() {
                                        _loading = false;
                                      });
                                    },
                                  ),
                                );
                              } else {
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  toShow = Center(
                                    child: CircularProgressIndicator(),
                                  );
                                } else {
                                  toShow = SizedBox(
                                    height: 0,
                                  );
                                }
                              }
                              return toShow;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            } else {
              return Container();
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _novaPesagem,
        child: _creatingPesagem
            ? SizedBox(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              )
            : Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
