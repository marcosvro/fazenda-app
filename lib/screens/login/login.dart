import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/screens/login/sign_in_form.dart';
import 'package:app_fazenda/screens/login/sign_up_form.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/theme/colors.dart' as colors;
import 'package:app_fazenda/utils/theme/theme.dart' as themes;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  void initState() {
    // Espera até estar com refresh token em mãos
    Future.delayed(Duration.zero, () {
      var authProvider = Provider.of<AuthProvider>(context, listen: false);
      var navegacaoProvider =
          Provider.of<NavegacaoHomeProvider>(context, listen: false);
      authProvider.bloc.refreshToken.listen((token) {
        if (token?.status == Status.COMPLETED &&
            navegacaoProvider.index == INDEX_LOGIN) {
          navegacaoProvider.setNavegacao(INDEX_FAZENDA);
        } else if (token?.status == Status.ERROR &&
            (token?.message ?? "") != "") {
          this._scaffoldKey.currentState?.showSnackBar(SnackBar(
                content: Text(token.message),
                behavior: SnackBarBehavior.floating,
                duration: Duration(seconds: 4),
              ));
        }
      });
    });

    super.initState();
  }

  Widget signForms() {
    return Consumer<AuthProvider>(
      builder: (context, provider, child) {
        if (provider.index == INDEX_CREATE_ACCOUNT) {
          return SignUpForm();
        } else {
          return SignInForm();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = themes.authPagesTheme;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));

    return Theme(
      data: theme,
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: colors.PRIMARY_GRADIENT_VERTICAL,
            ),
          ),
          Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.transparent,
            body: signForms(),
          ),
        ],
      ),
    );
  }
}
