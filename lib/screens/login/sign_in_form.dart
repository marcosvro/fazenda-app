import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/utils/helpers/validators.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class SignInForm extends StatefulWidget {
  @override
  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  BehaviorSubject<bool> _emailValidSubject = BehaviorSubject<bool>(),
      _passwordValidSubject = BehaviorSubject<bool>();

  bool _emailAutoValidate = false, _passwordAutoValidate = false;

  bool _isLoading = false;
  GlobalKey<FormState> _formKey = new GlobalKey();

  @override
  void initState() {
    _enableFieldsAutoValidate();
    super.initState();
  }

  _enableFieldsAutoValidate() {
    _emailController.addListener(() {
      if (!_emailAutoValidate && (_emailController.text.length > 0))
        setState(() {
          _emailAutoValidate = true;
        });
    });
    _passwordController.addListener(() {
      if (!_passwordAutoValidate && (_passwordController.text.length > 0))
        setState(() {
          _passwordAutoValidate = true;
        });
    });
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _emailValidSubject.close();
    _passwordValidSubject.close();
    super.dispose();
  }

  UnderlineInputBorder _getInputBorder(bool condition) {
    return condition
        ? UnderlineInputBorder(borderSide: BorderSide(color: Colors.green))
        : UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey));
  }

  UnderlineInputBorder _getErrorBorder(bool condition) {
    return condition
        ? UnderlineInputBorder(borderSide: BorderSide(color: Colors.red))
        : UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey));
  }

  Color _getLabelColor(bool condition, int textLength) {
    if (textLength == 0) return Colors.grey;
    return condition ? Colors.green : Colors.red;
  }

  String _validateEmail(String value) {
    final validator = validateEmail(value);

    if (validator == null)
      _emailValidSubject.add(true);
    else
      _emailValidSubject.add(false);

    return validator;
  }

  String _validatePassword(String value) {
    final validator = validatePassword(value);

    if (validator == null)
      _passwordValidSubject.add(true);
    else
      _passwordValidSubject.add(false);

    return validator;
  }

  Widget _getInputField({
    @required String label,
    @required bool autoValidateFlag,
    @required Stream streamValidateFlag,
    @required String Function(String) validateFunc,
    TextEditingController controller,
    TextInputType keyboadType,
    List<TextInputFormatter> inputFormatters,
    bool isPassword = false,
    bool optional = false,
  }) {
    return StreamBuilder(
      initialData: false,
      stream: streamValidateFlag,
      builder: (_, snapshot) {
        return TextFormField(
          style: TextStyle(
            color: Colors.grey[700],
            fontWeight: FontWeight.w600,
          ),
          autovalidate: autoValidateFlag,
          controller: controller,
          decoration: InputDecoration(
            labelText: label,
            enabledBorder: _getInputBorder(snapshot.data),
            focusedBorder: _getInputBorder(snapshot.data),
            errorBorder: _getErrorBorder(
                (optional && (controller?.text?.length ?? 0) > 0) || !optional),
            labelStyle: TextStyle(
                color: _getLabelColor(
                    snapshot.data,
                    (controller?.text?.length ?? 0) == 0 && autoValidateFlag
                        ? (optional ? 0 : 1)
                        : controller?.text?.length ?? 0)),
          ),
          keyboardType: keyboadType ?? TextInputType.text,
          textCapitalization: TextCapitalization.none,
          inputFormatters: inputFormatters ?? null,
          obscureText: isPassword,
          validator: validateFunc,
        );
      },
    );
  }

  Future<void> _submit(BuildContext context) async {
    String error;
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    if (_formKey.currentState.validate()) {
      await authProvider.bloc.fetchSignIn(
        _emailController.text,
        _passwordController.text,
      );
    } else {
      error = 'Estamos quase lá! Mas alguns campos ainda contêm erros.';
    }
    if (error != null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(error),
        behavior: SnackBarBehavior.floating,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: ListView(
            shrinkWrap: true,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "LOGAR",
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 18,
                      letterSpacing: 0.5,
                    ),
                  ),
                  SizedBox(height: 30),
                  Card(
                    color: PRIMARY_SWATCH[50],
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          _getInputField(
                            label: 'E-mail',
                            autoValidateFlag: _emailAutoValidate,
                            streamValidateFlag: _emailValidSubject,
                            validateFunc: _validateEmail,
                            controller: _emailController,
                          ),
                          SizedBox(height: 5),
                          _getInputField(
                            label: 'Senha',
                            autoValidateFlag: _passwordAutoValidate,
                            streamValidateFlag: _passwordValidSubject,
                            validateFunc: _validatePassword,
                            controller: _passwordController,
                            isPassword: true,
                          ),
                          SizedBox(height: 5),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Container(
                              height: 42,
                              width: 230,
                              child: PillGradientButton(
                                "LOGAR",
                                () async {
                                  setState(() {
                                    _isLoading = true;
                                  });
                                  await _submit(context);
                                  setState(() {
                                    _isLoading = false;
                                  });
                                },
                                loading: _isLoading,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Container(
                      height: 42,
                      width: 230,
                      child: PillOutlineButton(
                        "CRIAR CONTA",
                        () async {
                          AuthProvider provider =
                              Provider.of(context, listen: false);
                          provider.setNavegacao(INDEX_CREATE_ACCOUNT);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
