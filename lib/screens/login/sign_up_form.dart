import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/validators.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();
  final TextEditingController _foneController = TextEditingController();
  final TextEditingController _inviteCodeController = TextEditingController();

  BehaviorSubject<bool> _nameValidSubject = BehaviorSubject<bool>(),
      _emailValidSubject = BehaviorSubject<bool>(),
      _passwordValidSubject = BehaviorSubject<bool>(),
      _confirmPasswordValidSubject = BehaviorSubject<bool>(),
      _foneValidSubject = BehaviorSubject<bool>(),
      _inviteCodeSubject = BehaviorSubject<bool>();

  bool _nameAutoValidate = false,
      _emailAutoValidate = false,
      _passwordAutoValidate = false,
      _confirmPasswordAutoValidate = false,
      _foneAutoValidate = false,
      _inviteCodeAutoValidade = false;

  bool _isLoading = false;
  GlobalKey<FormState> _formKey = new GlobalKey();

  var foneMask = new MaskTextInputFormatter(
      mask: '(##) #####-####', filter: {"#": RegExp(r'[0-9]')});

  @override
  void initState() {
    _enableFieldsAutoValidate();
    super.initState();
  }

  _enableFieldsAutoValidate() {
    _nameController.addListener(() {
      if (!_nameAutoValidate && (_nameController.text.length > 0))
        setState(() {
          _nameAutoValidate = true;
        });
    });
    _emailController.addListener(() {
      if (!_emailAutoValidate && (_emailController.text.length > 0))
        setState(() {
          _emailAutoValidate = true;
        });
    });
    _passwordController.addListener(() {
      if (!_passwordAutoValidate && (_passwordController.text.length > 0))
        setState(() {
          _passwordAutoValidate = true;
        });
    });
    _confirmPasswordController.addListener(() {
      if (!_confirmPasswordAutoValidate &&
          (_confirmPasswordController.text.length > 0))
        setState(() {
          _confirmPasswordAutoValidate = true;
        });
    });
    _foneController.addListener(() {
      if (!_foneAutoValidate && (_foneController.text.length > 0))
        setState(() {
          _foneAutoValidate = true;
        });
      else if (_foneAutoValidate && _foneController.text.length == 0)
        setState(() {
          _foneAutoValidate = false;
        });
    });
    _inviteCodeController.addListener(() {
      if (!_inviteCodeAutoValidade && _inviteCodeController.text.length > 0) {
        setState(() {
          _inviteCodeAutoValidade = true;
        });
      }
    });
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    _foneController.dispose();
    _inviteCodeController.dispose();
    _emailValidSubject.close();
    _passwordValidSubject.close();
    _confirmPasswordValidSubject.close();
    _foneValidSubject.close();
    _inviteCodeSubject.close();
    super.dispose();
  }

  UnderlineInputBorder _getInputBorder(bool condition) {
    return condition
        ? UnderlineInputBorder(borderSide: BorderSide(color: Colors.green))
        : UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey));
  }

  UnderlineInputBorder _getErrorBorder(bool condition) {
    return condition
        ? UnderlineInputBorder(borderSide: BorderSide(color: Colors.red))
        : UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey));
  }

  Color _getLabelColor(bool condition, int textLength) {
    if (textLength == 0) return Colors.grey;
    return condition ? Colors.green : Colors.red;
  }

  String _validateName(String value) {
    final validator = validateNotEmpty(value);

    if (validator == null)
      _nameValidSubject.add(true);
    else
      _nameValidSubject.add(false);

    return validator;
  }

  String _validateEmail(String value) {
    final validator = validateEmail(value);

    if (validator == null)
      _emailValidSubject.add(true);
    else
      _emailValidSubject.add(false);

    return validator;
  }

  String _validatePassword(String value) {
    final validator = validatePassword(value);

    if (validator == null)
      _passwordValidSubject.add(true);
    else
      _passwordValidSubject.add(false);

    return validator;
  }

  String _validateConfirmPassword(String value) {
    String validator;
    if (_passwordController.text.length == 0)
      validator = "";
    else
      validator = _passwordController.text == value
          ? null
          : "As senhas precisam coincidir";

    if (validator == null)
      _confirmPasswordValidSubject.add(true);
    else
      _confirmPasswordValidSubject.add(false);

    return validator;
  }

  String _validateFone(String value) {
    var validator =
        _foneController.text.length == 15 || _foneController.text.length == 0
            ? null
            : "";

    if (validator == null)
      _foneValidSubject.add(true);
    else
      _foneValidSubject.add(false);

    return validator;
  }

  String _validateInviteCode(String value) {
    var validator;

    if (validator == null)
      _inviteCodeSubject.add(true);
    else
      _inviteCodeSubject.add(false);

    return validator;
  }

  Widget _getInputField({
    @required String label,
    @required bool autoValidateFlag,
    @required Stream streamValidateFlag,
    @required String Function(String) validateFunc,
    TextEditingController controller,
    TextInputType keyboadType,
    List<TextInputFormatter> inputFormatters,
    bool isPassword = false,
    bool optional = false,
  }) {
    return StreamBuilder(
      initialData: false,
      stream: streamValidateFlag,
      builder: (_, snapshot) {
        return TextFormField(
          style: TextStyle(
            color: Colors.grey[700],
            fontWeight: FontWeight.w600,
          ),
          autovalidate: autoValidateFlag,
          controller: controller,
          decoration: InputDecoration(
            labelText: label,
            enabledBorder: _getInputBorder(snapshot.data),
            focusedBorder: _getInputBorder(snapshot.data),
            errorBorder: _getErrorBorder(
                (optional && (controller?.text?.length ?? 0) > 0) || !optional),
            labelStyle: TextStyle(
                color: _getLabelColor(
                    snapshot.data,
                    (controller?.text?.length ?? 0) == 0 && autoValidateFlag
                        ? (optional ? 0 : 1)
                        : controller?.text?.length ?? 0)),
          ),
          keyboardType: keyboadType ?? TextInputType.text,
          textCapitalization: TextCapitalization.none,
          inputFormatters: inputFormatters ?? null,
          obscureText: isPassword,
          validator: validateFunc,
        );
      },
    );
  }

  Future<void> _submit(BuildContext context) async {
    String error;
    var authProvider = Provider.of<AuthProvider>(context, listen: false);
    if (_formKey.currentState.validate()) {
      await authProvider.bloc.fetchSignUp(
        _emailController.text,
        _passwordController.text,
        _nameController.text,
        (_inviteCodeController.text ?? "") == ""
            ? null
            : _inviteCodeController.text,
      );
    } else {
      error = 'Estamos quase lá! Mas alguns campos ainda contêm erros.';
    }
    if (error != null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(error),
        behavior: SnackBarBehavior.floating,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: ListView(
            shrinkWrap: true,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "CADASTRAR",
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.white,
                      fontSize: 18,
                      letterSpacing: 0.5,
                    ),
                  ),
                  SizedBox(height: 30),
                  Card(
                    color: PRIMARY_SWATCH[50],
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          _getInputField(
                            label: 'Nome',
                            autoValidateFlag: _nameAutoValidate,
                            streamValidateFlag: _nameValidSubject,
                            validateFunc: _validateName,
                            controller: _nameController,
                          ),
                          SizedBox(height: 5),
                          _getInputField(
                            label: 'E-mail',
                            autoValidateFlag: _emailAutoValidate,
                            streamValidateFlag: _emailValidSubject,
                            validateFunc: _validateEmail,
                            controller: _emailController,
                          ),
                          SizedBox(height: 5),
                          _getInputField(
                            label: 'Senha',
                            autoValidateFlag: _passwordAutoValidate,
                            streamValidateFlag: _passwordValidSubject,
                            validateFunc: _validatePassword,
                            controller: _passwordController,
                            isPassword: true,
                          ),
                          SizedBox(height: 5),
                          _getInputField(
                            label: 'Confirme a senha',
                            autoValidateFlag: _confirmPasswordAutoValidate,
                            streamValidateFlag: _confirmPasswordValidSubject,
                            validateFunc: _validateConfirmPassword,
                            controller: _confirmPasswordController,
                            isPassword: true,
                          ),
                          SizedBox(height: 5),
                          _getInputField(
                            label: 'Telefone (Opcinal)',
                            autoValidateFlag: _foneAutoValidate,
                            streamValidateFlag: _foneValidSubject,
                            validateFunc: _validateFone,
                            inputFormatters: [foneMask],
                            keyboadType: TextInputType.number,
                            controller: _foneController,
                            optional: true,
                          ),
                          SizedBox(height: 5),
                          _getInputField(
                            label: 'Código de convite (Opcinal)',
                            autoValidateFlag: _inviteCodeAutoValidade,
                            streamValidateFlag: _inviteCodeSubject,
                            validateFunc: _validateInviteCode,
                            controller: _inviteCodeController,
                            optional: true,
                          ),
                          SizedBox(height: 5),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Container(
                              height: 42,
                              width: 230,
                              child: PillGradientButton(
                                "CADASTRAR",
                                () async {
                                  setState(() {
                                    _isLoading = true;
                                  });
                                  await _submit(context);
                                  setState(() {
                                    _isLoading = false;
                                  });
                                },
                                loading: _isLoading,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Container(
                      height: 42,
                      width: 230,
                      child: PillOutlineButton(
                        "JÁ POSSUI UMA CONTA?",
                        () async {
                          AuthProvider provider =
                              Provider.of(context, listen: false);
                          provider.setNavegacao(INDEX_LOGIN_ACCOUNT);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
