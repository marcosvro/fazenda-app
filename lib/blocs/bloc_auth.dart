import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/models/token.dart';
import 'package:app_fazenda/models/user.dart';
import 'package:app_fazenda/repositories/auth_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

const FAZENDA_PREFS_KEY = "fazenda";
const REFRESH_TOKEN_PREFS_KEY = "refresh_token";
const ACCESS_TOKEN_PREFS_KEY = "access_token";

class AuthBloc {
  final AuthRepository repository = AuthRepository();
  final _refreshToken = BehaviorSubject<ApiResponse<RefreshToken>>();
  final _fazenda = BehaviorSubject<ApiResponse<Fazenda>>();
  final _accessToken = BehaviorSubject<ApiResponse<AccessToken>>();

  Stream<ApiResponse<RefreshToken>> get refreshToken => _refreshToken.stream;
  Stream<ApiResponse<Fazenda>> get fazenda => _fazenda.stream;
  Stream<ApiResponse<AccessToken>> get accessToken => _accessToken.stream;
  Function(ApiResponse<RefreshToken>) get changeRefreshToken =>
      _refreshToken.sink.add;
  Function(ApiResponse<Fazenda>) get changeFazenda => _fazenda.sink.add;
  Function(ApiResponse<AccessToken>) get changeAccessToken =>
      _accessToken.sink.add;
  Fazenda get fazendaObject => _fazenda.value?.data;
  AccessToken get accessTokenObject => _accessToken.value?.data;

  saveRefreshToken(RefreshToken refreshToken) {
    var refreshTokenString = refreshToken.toJson();
    savePreference(refreshToken: refreshTokenString);
  }

  saveFazenda(Fazenda fazendaJsom) {
    var fazendaString = fazendaJsom.toJson();
    savePreference(fazenda: fazendaString);
  }

  saveAccessToken(AccessToken accessToken) {
    var accessTokenString = accessToken.toJson();
    savePreference(accessToken: accessTokenString);
  }

  savePreference(
      {String refreshToken, String fazenda, String accessToken}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (refreshToken != null) {
      prefs.setString(REFRESH_TOKEN_PREFS_KEY, refreshToken);
    }
    if (accessToken != null) {
      prefs.setString(ACCESS_TOKEN_PREFS_KEY, accessToken);
    }
    if (fazenda != null) {
      prefs.setString(FAZENDA_PREFS_KEY, fazenda);
    }
  }

  loadPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String hasRefreshToken = prefs.getString(REFRESH_TOKEN_PREFS_KEY);
    String hasFazenda = prefs.getString(FAZENDA_PREFS_KEY);
    String hasAccessToken = prefs.getString(ACCESS_TOKEN_PREFS_KEY);

    if (hasRefreshToken != null) {
      // changeRefreshToken(
      //     ApiResponse.error("Usuário encontrado mas estou aqui pq quero!"));
      var refreshToken = RefreshToken.fromJson(hasRefreshToken);
      changeRefreshToken(ApiResponse.completed(refreshToken));
    } else {
      changeRefreshToken(
          ApiResponse.error("Nenhum usuário logado neste dispositivo."));
      return;
    }
    if (hasFazenda != null) {
      var fazenda = Fazenda.fromJson(hasFazenda);
      changeFazenda(ApiResponse.completed(fazenda));
    } else {
      changeFazenda(
          ApiResponse.error("Selecione uma fazenda para prosseguir."));
    }

    if (hasAccessToken != null) {
      var accessToken = AccessToken.fromJson(hasAccessToken);
      changeAccessToken(ApiResponse.completed(accessToken));
    } else {
      changeAccessToken(ApiResponse.error(""));
    }
  }

  fetchSignIn(String email, String password) async {
    changeRefreshToken(ApiResponse.loading('Fazendo login'));
    try {
      RefreshToken refreshToken =
          await repository.getRefreshToken(email, password);
      saveRefreshToken(refreshToken);
      changeRefreshToken(ApiResponse.completed(refreshToken));
    } catch (e) {
      if (e.runtimeType == ServiceHandableException) {
        print(
            "Error on fetchFazendaToken: ${(e as ServiceHandableException).message}");
      } else {
        print("Error on fetchFazendaToken: ${e.toString()}");
      }
      handleErrors(e);
    }
  }

  fetchFazendaToken(Fazenda fazenda) async {
    changeRefreshToken(ApiResponse.loading('Fazendo login'));
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String refreshTokenString = prefs.getString(REFRESH_TOKEN_PREFS_KEY);
      if (refreshTokenString == null) {
        this.logOut();
      }
      var refreshToken = RefreshToken.fromJson(refreshTokenString);
      AccessToken accessToken = await repository.getAccessToken(
          refreshToken.refreshToken, fazenda.id);
      saveFazenda(fazenda);
      saveAccessToken(accessToken);
      changeAccessToken(ApiResponse.completed(accessToken));
      changeFazenda(ApiResponse.completed(fazenda));
    } catch (e) {
      if (e.runtimeType == ServiceHandableException) {
        print(
            "Error on fetchFazendaToken: ${(e as ServiceHandableException).message}");
        changeFazenda(
            ApiResponse.error((e as ServiceHandableException).message));
        changeAccessToken(
            ApiResponse.error((e as ServiceHandableException).message));
      } else {
        changeFazenda(ApiResponse.error(""));
        changeAccessToken(ApiResponse.error(""));
        print("Error on fetchFazendaToken: ${e.toString()}");
      }
    }
  }

  fetchSignUp(String email, String password, String username,
      String inviteRefreshToken) async {
    changeRefreshToken(ApiResponse.loading('Criando usuário'));
    try {
      User user = await repository.createUser(
          email, password, username, inviteRefreshToken);
      if (user.id != null) {
        await fetchSignIn(email, password);
      } else {
        handleErrors(Exception(
            "Ocorreu um erro na criação do usuário. Tente novamente mais tarde."));
      }
    } catch (e) {
      print("Error on fetchSignUp: ${e.toString()}");
      handleErrors(e);
    }
  }

  logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(REFRESH_TOKEN_PREFS_KEY);
    prefs.remove(FAZENDA_PREFS_KEY);
    prefs.remove(ACCESS_TOKEN_PREFS_KEY);
    changeRefreshToken(ApiResponse.error(""));
    changeFazenda(ApiResponse.error(""));
    changeAccessToken(ApiResponse.error(""));
  }

  trocarFazenda() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(FAZENDA_PREFS_KEY);
    prefs.remove(ACCESS_TOKEN_PREFS_KEY);
    changeFazenda(ApiResponse.error(""));
    changeAccessToken(ApiResponse.error(""));
  }

  dispose() {
    _refreshToken.close();
    _fazenda.close();
    _accessToken.close();
  }

  handleErrors(dynamic e) {
    if (e.toString().contains('SocketException')) {
      changeRefreshToken(ApiResponse.error('Sem conexão com a internet'));
    } else if (e.runtimeType == ServiceHandableException) {
      changeRefreshToken(
          ApiResponse.error((e as ServiceHandableException).message));
    }
  }
}
