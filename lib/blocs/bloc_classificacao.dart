import 'package:app_fazenda/models/animal.dart';
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/models/lote.dart';
import 'package:app_fazenda/models/pesagem.dart';
import 'package:app_fazenda/models/token.dart';
import 'package:app_fazenda/repositories/animal_repository.dart';
import 'package:app_fazenda/repositories/lote_repository.dart';
import 'package:app_fazenda/repositories/pesagem_repository.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';

class ClassificacaoBloc {
  final LoteRepository loteRepository = LoteRepository();
  final AnimalRepository animalRepository = AnimalRepository();
  List<Lote> lotes;
  Pesagem lastPesagem;
  final PesagemRepository pesagemRepository = PesagemRepository();

  Future<String> fetchLotes(AccessToken token, Fazenda fazenda) async {
    try {
      List<Lote> lotesData;
      Pesagem lastPesagemData;
      var resp = await Future.wait([
        loteRepository
            .getLoteAnimais(fazendaID: fazenda.id, token: token)
            .then((value) => lotesData = value),
        pesagemRepository
            .getLastPesagem(token: token)
            .then((value) => lastPesagemData = value)
      ]);

      lastPesagem = lastPesagemData;
      lotes = lotesData;

      if (lotes != null &&
          lastPesagem != null &&
          lastPesagem.medicoes != null) {
        print(lotes.length);
        for (var i = 0; i < lotes.length; i++) {
          print(lotes[i].animais.length);
          for (var j = 0; j < lotes[i].animais.length; j++) {
            print(lotes[i].animais[j].id);
            lotes[i].animais[j].ultimaMedicao = lastPesagem.medicoes
                .firstWhere(
                    (element) => element.vacaId == lotes[i].animais[j].id,
                    orElse: () => null)
                ?.peso;
            print(lotes[i].animais[j].ultimaMedicao);
          }
        }
      }

      return null;
    } catch (e) {
      if (e.runtimeType == ServiceHandableException) {
        print(
            "Error on ClassificacaoBloc.fetchLotes: ${(e as ServiceHandableException).message}");
        lotes = null;
        return (e as ServiceHandableException).message;
      } else {
        print("Error on ClassificacaoBloc.fetchLotes: ${e.toString()}");
        lotes = null;
        return "Ocorreu um erro, não foi possível obter a lista de lotes.";
      }
    }
  }

  Future<String> moveAnimalBetweenLotes(
      AccessToken token, Animal animal, Lote newLote, int newLoteIndex) async {
    try {
      var moved = await animalRepository.updateAnimal(
          animalID: animal.id,
          token: token,
          loteID: newLote.id,
          forceUpdateLoteID: true);
      if (moved) {
        lotes[lotes.indexWhere((l) => l.id == animal.loteId)]
            .animais
            .removeWhere((a) => a.id == animal.id);
        animal.loteId = newLote.id;
        lotes[lotes.indexWhere((l) => l.id == newLote.id)]
            .animais
            .insert(newLoteIndex, animal);
        return null;
      } else {
        var err = "Não foi possível alterar o lote deste animal";
        print(err);
        return err;
      }
    } catch (e) {
      print(
          "Error on ClassificacaoBloc.moveAnimalBetweenLotes: ${e.toString()}");
      return "Não foi possível alterar o lote deste animal";
    }
  }
}
