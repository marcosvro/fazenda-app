import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class SearchQuery {
  String key;
  String value;

  SearchQuery({@required this.key, @required this.value});
}

class SearchBloc {
  final _searchQuery = BehaviorSubject<SearchQuery>();
  Stream<SearchQuery> get searchQuery => _searchQuery.stream;
  Function(SearchQuery) get changeSearchQuery => _searchQuery.sink.add;

  void dispose() {
    _searchQuery.close();
  }
}
