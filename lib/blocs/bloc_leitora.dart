import 'dart:async';

import 'package:LFReader2/LFReader2.dart';
import 'package:app_fazenda/common/common_bluetooth_conect.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:rxdart/rxdart.dart';

class LeitoraBloc {
  BluetoothDevice dispositivo;
  LFReader2ConnectionState status;
  final _statusSubject = BehaviorSubject<LFReader2ConnectionState>();
  Stream<LFReader2ConnectionState> get statusStream => _statusSubject.stream;
  Function(LFReader2ConnectionState) get changeStatus =>
      _statusSubject.sink.add;

  StreamSubscription<Tag> _readtagSubscription;
  StreamSubscription<LFReader2ConnectionState> _connectSubscription;

  bool isScaningTag = false;
  bool isConnecting = false;

  void dispose() {
    _statusSubject.close();
  }

  LeitoraBloc() {
    LFReader2.instance.connectionStream.listen((event) {
      status = event;
      changeStatus(event);
    });
  }

  setDispositivo(BluetoothDevice device) {
    dispositivo = device;
  }

  Future<bool> connectLeitora(BluetoothDevice device) {
    if (isConnecting) return null;

    Completer c = new Completer<bool>();
    isConnecting = true;
    status = LFReader2ConnectionState.UNKNOWN;
    LFReader2.instance.connectDevice(device.address);
    _connectSubscription = statusStream.listen((event) async {
      if (status != LFReader2ConnectionState.UNKNOWN) {
        dispositivo =
            status == LFReader2ConnectionState.CONNECTED ? device : null;
        isConnecting = false;
        if (_connectSubscription != null) {
          await _connectSubscription.cancel();
          _connectSubscription = null;
        }

        c.complete(status == LFReader2ConnectionState.CONNECTED);
      }
    });
    return c.future;
  }

  Future<Tag> scanTag({bool ignoreConnect = false}) async {
    if (dispositivo == null || isScaningTag) return null;

    if (!ignoreConnect && !(await connectLeitora(dispositivo))) {
      return null;
    }

    isScaningTag = true;
    Completer c = new Completer<Tag>();

    LFReader2.instance.startRead();
    _readtagSubscription = LFReader2.instance.tagStream.listen((tag) {
      print("Tag received: ${tag.toMap()}");
      if (_readtagSubscription != null) {
        _readtagSubscription.cancel();
        _readtagSubscription = null;
      }
      isScaningTag = false;
      c.complete(tag.serial == "" ? null : tag);
    });

    return c.future;
  }
}
