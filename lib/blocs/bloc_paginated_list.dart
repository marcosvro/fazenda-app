import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';
import 'package:app_fazenda/blocs/bloc_search.dart';

class PageConfig {
  int page;
  int size;

  PageConfig({@required this.page, @required this.size});

  PageConfig next() {
    return new PageConfig(page: page + 1, size: size);
  }
}

class PaginatedListBloc<T> {
  static const int PAGE_SIZE = 10;

  final _list = BehaviorSubject<List<T>>();
  Stream<List<T>> get list => _list.stream;
  Function(List<T>) get changeList => _list.sink.add;
  final Future<List<T>> Function(PageConfig, SearchQuery) getList;

  SearchQuery currentQuery;
  PageConfig currentPage;
  bool _loadingList = false;

  PaginatedListBloc(this.getList);

  Future<void> fetchList(
      {RefreshController controller, SearchQuery query}) async {
    if (_loadingList) return;
    _loadingList = true;
    bool success = false;
    bool hasData = false;
    try {
      if (query != null) {
        if ((query?.key ?? "") != (currentQuery?.key ?? "") ||
            (query?.value ?? "") != (currentQuery?.value ?? "")) {
          currentQuery = new SearchQuery(key: query.key, value: query.value);
          currentPage = null;
        }
      } else {
        if (currentQuery != null) {
          currentQuery = null;
          currentPage = null;
        }
      }

      var nextPage = currentPage == null
          ? new PageConfig(page: 1, size: PAGE_SIZE)
          : currentPage.next();

      List<T> list = await getList(nextPage, currentQuery);

      success = list != null;
      // att page
      if ((list?.length ?? 0) != 0) {
        hasData = true;
        if (currentPage == null) {
          currentPage = new PageConfig(page: 1, size: PAGE_SIZE);
          if (!_list.isClosed) changeList(list);
        } else {
          currentPage.page = currentPage.page + 1;
          if (!_list.isClosed) changeList([..._list.value, ...list]);
        }
      } else if (list != null && list.length == 0 && currentPage == null) {
        if (!_list.isClosed) changeList(list);
      }
    } catch (e, c) {
      print("Exception throw on PaginatedListBloc.fetchList : $e \n$c");
      if (!_list.isClosed) changeList(null);
    }

    if (controller != null) {
      if (hasData) {
        controller.loadComplete();
      } else if (success) {
        controller.loadNoData();
      } else {
        controller.loadFailed();
      }
      //Workaround so its not stucked on the failed message.
      Timer(Duration(seconds: 3), () => controller.loadComplete());
    }

    _loadingList = false;
  }

  void dispose() {
    _list.close();
  }
}
