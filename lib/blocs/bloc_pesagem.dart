import 'dart:convert';

import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/models/pesagem.dart';
import 'package:app_fazenda/models/token.dart';
import 'package:app_fazenda/repositories/pesagem_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

const PESAGEM_PREFS_KEY = "pesagem";

class PesagemBloc {
  final PesagemRepository repository = PesagemRepository();
  final _pesagem = BehaviorSubject<ApiResponse<Pesagem>>();
  Stream<ApiResponse<Pesagem>> get pesagem => _pesagem.stream;
  Function(ApiResponse<Pesagem>) get changePesagem => _pesagem.sink.add;

  fetchPesagem(AccessToken token, Fazenda fazenda) async {
    changePesagem(ApiResponse.loading('Cadastrando nova pesagem'));
    try {
      Pesagem pesagem = await getPesagemFromStorage();

      if (pesagem == null || pesagem.finalizada)
        pesagem =
            await repository.createPesagem(fazendaID: fazenda.id, token: token);

      await savePesagemStorage(pesagem);
      changePesagem(ApiResponse.completed(pesagem));
    } catch (e) {
      if (e.runtimeType == ServiceHandableException) {
        print(
            "Error on PesagemBloc.fetchPesagem: ${(e as ServiceHandableException).message}");
        changePesagem(
            ApiResponse.error((e as ServiceHandableException).message));
      } else {
        changePesagem(ApiResponse.error(""));
        print("Error on PesagemBloc.fetchPesagem: ${e.toString()}");
      }
    }
  }

  Future<String> finalizaPesagem(AccessToken token, Fazenda fazenda) async {
    var pesagem = _pesagem.value;
    try {
      await repository.finalizaPesagem(
          pesagemID: pesagem?.data?.id, token: token);
      await deletePesagemFromStorage();
      changePesagem(null);
      return null;
    } catch (e) {
      if (e.runtimeType == ServiceHandableException) {
        print(
            "Error on PesagemBloc.finalizaPesagem: ${(e as ServiceHandableException).message}");
        return (e as ServiceHandableException).message;
      } else {
        print("Error on PesagemBloc.finalizaPesagem: ${e.toString()}");
        return "Ocorreu um erro na finalização da pesagem";
      }
    }
  }

  Future<Pesagem> getPesagemFromStorage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String pesagemString = prefs.getString(PESAGEM_PREFS_KEY);
    if (pesagemString != null) {
      return Pesagem.fromJson(pesagemString);
    }
    return null;
  }

  Future<void> savePesagemStorage(Pesagem pesagem) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(PESAGEM_PREFS_KEY, pesagem.toJson());
  }

  Future<void> deletePesagemFromStorage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(PESAGEM_PREFS_KEY);
  }

  void dispose() {
    _pesagem.close();
  }
}
