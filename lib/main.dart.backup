import 'dart:io';

import 'package:LFReader2/LFReader2.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final Key key;
  MyHomePage({this.key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var maskFormatter = new MaskTextInputFormatter(
      mask: '##:##:##:##:##:##', filter: {"#": RegExp(r'[A-Z0-9a-z]')});
  TextEditingController _controller = new TextEditingController.fromValue(
      TextEditingValue(text: "98:D3:36:00:AD:CC"));
  bool isConnecting = false;
  bool isStartingRead = false;
  TextEditingController type_controller = new TextEditingController();
  TextEditingController serial_controller = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    LFReader2.instance.tagStream.listen((tag) {
      print("Tag received: ${tag.toMap()}");
      _updateTag(tag);
    });
    super.initState();
  }

  void _updateTag(Tag tag) {
    setState(() {
      type_controller.text = tag.type;
      serial_controller.text = tag.serial;
      isStartingRead = false;
      LFReader2.instance.cmd();
      if (tag.serial == "") showSnackBar();
    });
  }

  void showSnackBar() {
    _scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: Text('Ocorreu um erro na leitura..'),
        duration: Duration(seconds: 3),
        behavior: SnackBarBehavior.floating,
      ),
    );
  }

  void _connectDevice() async {
    if (isConnecting) return;
    setState(() {
      isConnecting = true;
    });
    await LFReader2.instance.connectDevice(_controller.text);
    setState(() {
      isConnecting = false;
    });
  }

  void _startRead() async {
    if (isStartingRead) return;
    setState(() {
      isStartingRead = true;
    });
    await LFReader2.instance.startRead();
    Future.delayed(const Duration(seconds: 20), () {
      if (isStartingRead) {
        _updateTag(new Tag());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Teste de conexão',
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: TextFormField(
                controller: _controller,
                decoration: const InputDecoration(
                  hintText: 'Enter mac ID',
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some mac ID';
                  }
                  return null;
                },
                inputFormatters: [maskFormatter],
              ),
            ),
            SizedBox(height: 10),
            SizedBox(
              width: 140,
              height: 40,
              child: FlatButton(
                color: Colors.blue,
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                padding: EdgeInsets.all(8.0),
                splashColor: Colors.blueAccent,
                onPressed: _connectDevice,
                child: isConnecting
                    ? SizedBox(
                        width: 25,
                        height: 25,
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                    : Text(
                        "CONECTAR",
                        style: TextStyle(fontSize: 15.0),
                      ),
              ),
            ),
            SizedBox(height: 10),
            SizedBox(
              height: 40,
              width: 140,
              child: FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                padding: EdgeInsets.all(8.0),
                splashColor: Colors.blueAccent,
                onPressed: _startRead,
                child: isStartingRead
                    ? SizedBox(
                        width: 25,
                        height: 25,
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                    : Text(
                        "INICIAR LEITURA",
                        style: TextStyle(fontSize: 15.0),
                      ),
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: TextFormField(
                readOnly: true,
                decoration: const InputDecoration(
                  labelText: 'Serial',
                  hintText: "Faça uma leitura para ver o ID serial",
                ),
                controller: serial_controller,
              ),
            ),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: TextFormField(
                readOnly: true,
                decoration: const InputDecoration(
                  labelText: 'Type',
                  hintText: "Faça uma leitura para ver o tipo",
                ),
                controller: type_controller,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
