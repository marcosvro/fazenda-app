import 'dart:convert';

class Animal {
  Animal({
    this.id,
    this.nome,
    this.dataNascimento,
    this.sexo,
    this.paiId,
    this.maeId,
    this.producao,
    this.fazendaId,
    this.numeroChip,
    this.numeroBrinco,
    this.loteId,
    this.proprio,
    this.status,
    this.ultimaMedicao,
    this.dataBaixa,
    this.motivoBaixa,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String nome;
  DateTime dataNascimento;
  String sexo;
  int paiId;
  int maeId;
  bool producao;
  int fazendaId;
  String numeroChip;
  int numeroBrinco;
  int loteId;
  bool proprio;
  String status;
  double ultimaMedicao;
  DateTime dataBaixa;
  String motivoBaixa;
  DateTime createdAt;
  DateTime updatedAt;

  factory Animal.fromJson(String str) => Animal.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Animal.fromMap(Map<String, dynamic> json) => Animal(
        id: json["id"] == null ? null : json["id"],
        nome: json["nome"] == null ? null : json["nome"],
        dataNascimento: json["data_nascimento"] == null
            ? null
            : DateTime.parse(json["data_nascimento"]).toLocal(),
        sexo: json["sexo"] == null ? null : json["sexo"],
        paiId: json["pai_id"] == null ? null : json["pai_id"],
        maeId: json["mae_id"] == null ? null : json["mae_id"],
        producao: json["producao"] == null ? null : json["producao"],
        fazendaId: json["fazenda_id"] == null ? null : json["fazenda_id"],
        numeroChip: json["numero_chip"] == null ? null : json["numero_chip"],
        numeroBrinco:
            json["numero_brinco"] == null ? null : json["numero_brinco"],
        loteId: json["lote_id"] == null ? null : json["lote_id"],
        proprio: json["proprio"] == null ? null : json["proprio"],
        status: json["status"] == null ? null : json["status"],
        ultimaMedicao:
            json["ultima_medicao"] == null ? null : json["ultima_medicao"],
        dataBaixa: json["data_baixa"] == null
            ? null
            : DateTime.parse(json["data_baixa"]).toLocal(),
        motivoBaixa: json["motivo_baixa"] == null ? null : json["motivo_baixa"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]).toLocal(),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]).toLocal(),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nome": nome == null ? null : nome,
        "data_nascimento":
            dataNascimento == null ? null : dataNascimento.toIso8601String(),
        "sexo": sexo == null ? null : sexo,
        "pai_id": paiId == null ? null : paiId,
        "mae_id": maeId == null ? null : maeId,
        "producao": producao == null ? null : producao,
        "fazenda_id": fazendaId == null ? null : fazendaId,
        "numero_chip": numeroChip == null ? null : numeroChip,
        "numero_brinco": numeroBrinco == null ? null : numeroBrinco,
        "lote_id": loteId == null ? null : loteId,
        "proprio": proprio == null ? null : proprio,
        "status": status == null ? null : status,
        "ultima_medicao": ultimaMedicao == null ? null : ultimaMedicao,
        "data_baixa": dataBaixa == null ? null : dataBaixa.toIso8601String(),
        "motivo_baixa": motivoBaixa == null ? null : motivoBaixa,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}
