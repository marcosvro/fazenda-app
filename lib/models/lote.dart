// To parse this JSON data, do
//
//     final Lote = LoteFromMap(jsonString);

import 'dart:convert';

import 'package:app_fazenda/models/animal.dart';

class Lote {
  Lote({
    this.id,
    this.nome,
    this.descricao,
    this.animais,
    this.producao,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String nome;
  String descricao;
  List<Animal> animais;
  bool producao;
  DateTime createdAt;
  DateTime updatedAt;

  factory Lote.fromJson(String str) => Lote.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Lote.fromMap(Map<String, dynamic> json) => Lote(
        id: json["id"] == null ? null : json["id"],
        nome: json["nome"] == null ? null : json["nome"],
        descricao: json["descricao"] == null ? null : json["descricao"],
        animais: json["animais"] == null
            ? null
            : List<Animal>.from(json["animais"].map((x) => Animal.fromMap(x))),
        producao: json["producao"] == null ? null : json["producao"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]).toLocal(),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]).toLocal(),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nome": nome == null ? null : nome,
        "descricao": descricao == null ? null : descricao,
        "animais": animais == null
            ? null
            : List<dynamic>.from(animais.map((x) => x.toMap())),
        "producao": producao == null ? null : producao,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}
