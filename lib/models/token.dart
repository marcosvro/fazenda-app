// To parse this JSON data, do
//
//     final token = tokenFromMap(jsonString);

import 'dart:convert';

class RefreshToken {
  RefreshToken({
    this.refreshToken,
  });

  String refreshToken;

  factory RefreshToken.fromJson(String str) =>
      RefreshToken.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RefreshToken.fromMap(Map<String, dynamic> json) => RefreshToken(
        refreshToken: json["refresh_token"],
      );

  Map<String, dynamic> toMap() => {
        "refresh_token": refreshToken,
      };
}

class AccessToken {
  AccessToken({
    this.accessToken,
  });

  String accessToken;

  factory AccessToken.fromJson(String str) =>
      AccessToken.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AccessToken.fromMap(Map<String, dynamic> json) => AccessToken(
        accessToken: json["access_token"],
      );

  Map<String, dynamic> toMap() => {
        "access_token": accessToken,
      };
}
