// To parse this JSON data, do
//
//     final user = userFromMap(jsonString);

import 'dart:convert';

class User {
  User({
    this.id,
    this.nome,
    this.telefone,
    this.email,
    this.emailVerificado,
  });

  int id;
  String nome;
  String telefone;
  String email;
  bool emailVerificado;

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> json) => User(
        id: json["id"],
        nome: json["nome"],
        telefone: json["telefone"],
        email: json["email"],
        emailVerificado: json["email_verificado"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "nome": nome,
        "telefone": telefone,
        "email": email,
        "email_verificado": emailVerificado,
      };
}
