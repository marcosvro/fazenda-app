// To parse this JSON data, do
//
//     final fazenda = fazendaFromMap(jsonString);

import 'dart:convert';

class Fazenda {
  Fazenda({
    this.id,
    this.name,
    this.city,
    this.area,
  });

  int id;
  String name;
  String city;
  int area;

  factory Fazenda.fromJson(String str) => Fazenda.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Fazenda.fromMap(Map<String, dynamic> json) => Fazenda(
        id: json["id"],
        name: json["name"],
        city: json["city"],
        area: json["area"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "city": city,
        "area": area,
      };
}
