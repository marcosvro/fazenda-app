// To parse this JSON data, do
//
//     final pesagem = pesagemFromMap(jsonString);

import 'dart:convert';

class Pesagem {
  Pesagem({
    this.id,
    this.finalizada,
    this.fazendaId,
    this.pesoTotal,
    this.qtdPesagens,
    this.medicoes,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  bool finalizada;
  int fazendaId;
  double pesoTotal;
  int qtdPesagens;
  List<PesagemIndividual> medicoes;
  DateTime createdAt;
  DateTime updatedAt;

  factory Pesagem.fromJson(String str) => Pesagem.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Pesagem.fromMap(Map<String, dynamic> json) => Pesagem(
        id: json["id"] == null ? null : json["id"],
        finalizada: json["finalizada"] == null ? null : json["finalizada"],
        fazendaId: json["fazenda_id"] == null ? null : json["fazenda_id"],
        pesoTotal: json["peso_total"] == null
            ? null
            : (json["peso_total"].runtimeType == double
                ? json["peso_total"]
                : (json["peso_total"] as int).toDouble()),
        qtdPesagens: json["qtd_pesagens"] == null ? null : json["qtd_pesagens"],
        medicoes: json["medicoes"] == null
            ? null
            : List<PesagemIndividual>.from(
                json["medicoes"].map((x) => PesagemIndividual.fromMap(x))),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]).toLocal().toLocal(),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]).toLocal().toLocal(),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "finalizada": finalizada == null ? null : finalizada,
        "fazenda_id": fazendaId == null ? null : fazendaId,
        "peso_total": pesoTotal == null ? null : pesoTotal,
        "qtd_pesagens": qtdPesagens == null ? null : qtdPesagens,
        "medicoes": medicoes == null
            ? null
            : List<dynamic>.from(medicoes.map((x) => x.toMap())),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}

class PesagemIndividual {
  PesagemIndividual({
    this.id,
    this.pesagemId,
    this.fazendaId,
    this.vacaId,
    this.peso,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int pesagemId;
  int fazendaId;
  int vacaId;
  double peso;
  DateTime createdAt;
  DateTime updatedAt;

  factory PesagemIndividual.fromJson(String str) =>
      PesagemIndividual.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PesagemIndividual.fromMap(Map<String, dynamic> json) =>
      PesagemIndividual(
        id: json["id"] == null ? null : json["id"],
        pesagemId: json["pesagem_id"] == null ? null : json["pesagem_id"],
        fazendaId: json["fazenda_id"] == null ? null : json["fazenda_id"],
        vacaId: json["vaca_id"] == null ? null : json["vaca_id"],
        peso: json["peso"] == null ? null : json["peso"].toDouble(),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]).toLocal(),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]).toLocal(),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "pesagem_id": pesagemId == null ? null : pesagemId,
        "fazenda_id": fazendaId == null ? null : fazendaId,
        "vaca_id": vacaId == null ? null : vacaId,
        "peso": peso == null ? null : peso,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}
