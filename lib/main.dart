import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/providers/config.provider.dart';
import 'package:app_fazenda/providers/leitora.provider.dart';
import 'package:app_fazenda/providers/navegacao.provider.dart';
import 'package:app_fazenda/screens/home/home.dart';
import 'package:app_fazenda/screens/login/login.dart';
import 'package:app_fazenda/screens/screen_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //FlutterStatusbarcolor.setStatusBarColor(Color.fromARGB(255, 11, 115, 57));
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AuthProvider>.value(
          value: AuthProvider(),
        ),
        ChangeNotifierProvider<NavegacaoHomeProvider>.value(
          value: NavegacaoHomeProvider(),
        ),
        ChangeNotifierProvider<ConfigProvider>.value(
          value: ConfigProvider(),
        ),
        ChangeNotifierProvider<LeitoraProvider>.value(
          value: LeitoraProvider(),
        ),
      ],
      child: MaterialApp(
          title: 'APP Fazenda',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              appBarTheme: AppBarTheme(
                color: Color.fromARGB(255, 39, 174, 96),
                elevation: 0,
                brightness: Brightness.dark,
              ),
              primaryColor: Colors.green[400],
              accentColor: Colors.greenAccent,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              fontFamily: 'Montserrat'),
          initialRoute: '/',
          routes: {
            '/': (context) => ScreenRouter(),
            //'/login': (context) => Login(),
          },
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('pt', 'BR')
          ]),
    );
  }
}
