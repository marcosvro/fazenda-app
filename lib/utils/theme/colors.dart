import 'dart:ui';

import 'package:flutter/material.dart';

const MaterialColor PRIMARY_SWATCH = MaterialColor(0xff27AE60, {
  50: Color(0xffE0FFE8),
  100: Color(0xffC9F5D7),
  200: Color(0xffB2EBC6),
  300: Color(0xff84D7A4),
  400: Color(0xff56C382),
  500: Color(0xff3FB971),
  // 600 is the primary color
  600: Color(0xff27AE60),
  700: Color(0xff239E57),
  800: Color(0xff20904F),
  900: Color(0xff1D8348),
});

const LinearGradient APP_BAR_GRADIENT = LinearGradient(
  begin: Alignment.bottomLeft,
  end: Alignment.topRight,
  stops: [0.15, 1],
  colors: [Color(0xFF434041), Color(0xFFB41007)],
);

const LinearGradient PRIMARY_GRADIENT_VERTICAL = LinearGradient(
  begin: Alignment.bottomCenter,
  end: Alignment.topCenter,
  stops: [0.5, 1],
  colors: [Color(0xff1D8348), Color(0xff27AE60)],
);

const LinearGradient PRIMARY_GRADIENT_HORIZONTAL = LinearGradient(
  begin: Alignment.centerLeft,
  end: Alignment.centerRight,
  stops: [0.5, 1],
  colors: [Color(0xff1D8348), Color(0xff27AE60)],
);

const LinearGradient SPOTIFY_GRADIENT = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    stops: [0, 1],
    colors: [Color(0xff1DB954), Color(0xff61D088)]);
