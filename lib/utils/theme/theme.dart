import 'package:flutter/material.dart';

import 'colors.dart';

const Brightness BRIGHTNESS = Brightness.light;

final ThemeData authPagesTheme = ThemeData(
  appBarTheme: AppBarTheme(brightness: Brightness.light),
  fontFamily: 'Roboto',
  brightness: Brightness.dark,
  primarySwatch: PRIMARY_SWATCH,
  accentColor: PRIMARY_SWATCH,
);

// MAIN THEMES

final ThemeData mainTheme = ThemeData(
  appBarTheme: AppBarTheme(brightness: Brightness.light),
  brightness: Brightness.light,
  primarySwatch: PRIMARY_SWATCH,
  accentColor: PRIMARY_SWATCH,
  textTheme: TextTheme(
    caption: TextStyle(
      color: Colors.grey[400],
      fontFamily: 'Roboto',
      letterSpacing: .75,
      fontSize: 10,
      fontWeight: FontWeight.w600,
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: Colors.grey[300],
    contentPadding: EdgeInsets.all(15),
    enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.grey[700], width: 2)),
    focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.grey[700], width: 2)),
    disabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.grey[700], width: 2)),
    labelStyle: TextStyle(
      color: Colors.grey,
      fontSize: 20,
    ),
  ),
);

final ThemeData mainThemeDark = ThemeData(
  appBarTheme:
      AppBarTheme(brightness: Brightness.dark, color: Colors.grey[900]),
  brightness: Brightness.dark,
  primarySwatch: PRIMARY_SWATCH,
  accentColor: PRIMARY_SWATCH,
  textTheme: TextTheme(
    caption: TextStyle(
      color: Colors.grey[400],
      fontFamily: 'Roboto',
      letterSpacing: .75,
      fontSize: 10,
      fontWeight: FontWeight.w600,
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: Colors.grey[800],
    contentPadding: EdgeInsets.all(15),
    enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2)),
    focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2)),
    disabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2)),
    labelStyle: TextStyle(
      color: Colors.grey[400],
      fontSize: 20,
    ),
  ),
);

// BOTTOM NAVIGATION BAR

final bottomNavigationBarTheme = mainTheme.copyWith(
  canvasColor: PRIMARY_SWATCH,
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    // showUnselectedLabels: true,
    unselectedItemColor: PRIMARY_SWATCH[300],
    selectedItemColor: Colors.white,
  ),
);

final bottomNavigationBarThemeDark = mainThemeDark.copyWith(
  canvasColor: Colors.grey[900],
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    // showUnselectedLabels: true,
    unselectedItemColor: Colors.white,
    selectedItemColor: PRIMARY_SWATCH,
  ),
);
