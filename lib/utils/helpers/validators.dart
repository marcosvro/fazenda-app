import 'package:flutter/services.dart';

const Pattern emailPattern =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
const Pattern usernamePattern = r'^[a-z-0-9_.]+$';
const Pattern namePattern = r'^[a-zA-Z-0-9_.& ]+$';
const Pattern numberPattern = r'^[1-9]{2}(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$';

bool _isEmpty(String str) {
  return str == null || str.trim().isEmpty;
}

String validatePassword(String password) {
  if (_isEmpty(password) || password.length < 8) {
    return 'A senha precisa ter ao menos 8 caracteres';
  }
  return null;
}

String validateNotEmpty(String str) {
  if (_isEmpty(str)) {
    return 'Esse campo é obrigatório';
  }
  return null;
}

String validateUsername(value) {
  RegExp regex = new RegExp(usernamePattern);
  if (!regex.hasMatch(value))
    return 'Somente letras minúsculas, números, "-", "_" e "."';
  else
    return null;
}

String validateName(value) {
  RegExp regex = new RegExp(namePattern);
  if (!regex.hasMatch(value))
    return 'Somente letras, números, " ", "&", "-", "_" e "."';
  else
    return null;
}

String validateEmail(value) {
  RegExp regex = new RegExp(emailPattern);
  if (!regex.hasMatch(value))
    return 'Insira um email válido';
  else
    return null;
}

String validatePhoneNumber(String value) {
  RegExp regex = new RegExp(numberPattern);
  final String str = value
      .toString()
      .replaceAll('(', '')
      .replaceAll(')', '')
      .replaceAll(' ', '');
  if (!regex.hasMatch(str))
    return 'Insira um número válido de telefone';
  else
    return null;
}

String validateEmailorUsername(value) {
  RegExp emailRegex = new RegExp(emailPattern);
  RegExp usernameRegex = new RegExp(usernamePattern);
  if (!emailRegex.hasMatch(value) && !usernameRegex.hasMatch(value))
    return 'Email ou nome de usuário inválido';
  else
    return null;
}

String validateConfirmationEmail({String email, String confirmationEmail}) {
  if (confirmationEmail.isEmpty) return "Campo obrigatório";
  if (confirmationEmail != email)
    return "Os e-mails não são iguais. Verifique novamente";

  return null;
}

class LowerCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toLowerCase(),
      selection: newValue.selection,
    );
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
