import 'dart:convert';

import 'package:app_fazenda/models/animal.dart';
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/environments/environment.dev.dart' as env;
import 'package:app_fazenda/models/lote.dart';
import 'package:app_fazenda/models/token.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class LoteRepository {
  Future<Lote> createLote({
    @required int fazendaID,
    @required AccessToken token,
    @required String nome,
    String descricao,
    bool producao,
  }) async {
    Map<String, dynamic> body = {
      "nome": nome,
      "fazenda_id": fazendaID,
    };

    if (descricao != null) {
      body["descricao"] = descricao;
    }
    if (producao != null) {
      body["producao"] = producao;
    }

    http.Response response = await http.post(
      "${env.API_BASE_URL}/lotes",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
      body: jsonEncode(body),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return Lote.fromJson(response.body);
    } else if (response.statusCode == 422) {
      if (response.body.contains("Duplicate entry")) {
        if (response.body.contains("numero_chip")) {
          throw ServiceHandableException(
              'Já existe um animal cadastrado com esse Número de Chip');
        } else if (response.body.contains("numero_brinco")) {
          throw ServiceHandableException(
              'Já existe um animal cadastrado com esse Número de Brinco');
        }
      }
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<List<Lote>> getLotes({
    @required int fazendaID,
    @required AccessToken token,
    String queryKey,
    String queryValue,
    int page,
    int pageSize,
  }) async {
    List<String> queryParams = [];
    if (page != null) queryParams.add("pagina=$page");
    if (pageSize != null) queryParams.add("limite=$pageSize");
    if (queryKey != null) queryParams.add("buscarPor=$queryKey");
    if (queryValue != null) queryParams.add("buscar=$queryValue");

    var url =
        "${env.API_BASE_URL}/lotes${queryParams.length != 0 ? '?' + queryParams.join('&') : ''}";

    http.Response response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
    );

    //print("Status: ${response.statusCode}\nbody:${response.body}");

    if (response.statusCode >= 200 && response.statusCode < 300) {
      List mapList = json.decode(response.body);
      return mapList.map<Lote>((e) => Lote.fromMap(e)).toList();
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<List<Lote>> getLoteAnimais({
    @required int fazendaID,
    @required AccessToken token,
  }) async {
    var url = "${env.API_BASE_URL}/lotes/animais";

    http.Response response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      List mapList = json.decode(utf8.decode(response.bodyBytes));
      return mapList.map<Lote>((e) => Lote.fromMap(e)).toList();
    } else {
      print('Erro ${response.statusCode}\n${response.body}');
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }
}
