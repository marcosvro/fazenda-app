import 'dart:convert';

import 'package:app_fazenda/models/animal.dart';
import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/environments/environment.dev.dart' as env;
import 'package:app_fazenda/models/token.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class AnimalRepository {
  Future<Animal> createAnimal({
    @required int fazendaID,
    @required AccessToken token,
    @required String nome,
    @required String numeroChip,
    @required String numeroBrinco,
    @required String sexo,
    DateTime dataNascimento,
    bool producao,
    String status,
  }) async {
    Map<String, dynamic> body = {
      "nome": nome,
      "sexo": sexo,
      "fazenda_id": fazendaID,
      "numero_chip": numeroChip,
      "numero_brinco": int.parse(numeroBrinco),
    };

    if (dataNascimento != null) {
      body["data_nascimento"] = dataNascimento.toUtc().toIso8601String();
    }
    if (producao != null) {
      body["producao"] = producao;
    }
    if (status != null) {
      body["status"] = status;
    }

    http.Response response = await http.post(
      "${env.API_BASE_URL}/animais",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
      body: jsonEncode(body),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return Animal.fromJson(response.body);
    } else if (response.statusCode == 422) {
      if (response.body.contains("Duplicate entry")) {
        if (response.body.contains("numero_chip")) {
          throw ServiceHandableException(
              'Já existe um animal cadastrado com esse Número de Chip');
        } else if (response.body.contains("numero_brinco")) {
          throw ServiceHandableException(
              'Já existe um animal cadastrado com esse Número de Brinco');
        }
      }
      throw ServiceHandableException('Ocorreu um erro.');
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<List<Animal>> getAnimais({
    @required int fazendaID,
    @required AccessToken token,
    String queryKey,
    String queryValue,
    int page,
    int pageSize,
  }) async {
    List<String> queryParams = [];
    if (page != null) queryParams.add("pagina=$page");
    if (pageSize != null) queryParams.add("limite=$pageSize");
    if (queryKey != null) queryParams.add("buscarPor=$queryKey");
    if (queryValue != null) queryParams.add("buscar=$queryValue");

    var url =
        "${env.API_BASE_URL}/animais${queryParams.length != 0 ? '?' + queryParams.join('&') : ''}";

    http.Response response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
    );

    //print("Status: ${response.statusCode}\nbody:${response.body}");

    if (response.statusCode >= 200 && response.statusCode < 300) {
      List mapList = json.decode(response.body);
      return mapList.map<Animal>((e) => Animal.fromMap(e)).toList();
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<bool> updateAnimal({
    @required AccessToken token,
    @required int animalID,
    String nome,
    String numeroChip,
    String numeroBrinco,
    String sexo,
    DateTime dataNascimento,
    bool producao,
    String status,
    int loteID,
    bool forceUpdateLoteID = false,
  }) async {
    Map<String, dynamic> body = {};

    if (nome != null) {
      body["nome"] = nome;
    }
    if (sexo != null) {
      body["sexo"] = sexo;
    }
    if (numeroChip != null) {
      body["numero_chip"] = numeroChip;
    }
    if (numeroBrinco != null) {
      body["numero_brinco"] = int.parse(numeroBrinco);
    }
    if (loteID != null || forceUpdateLoteID) {
      body['lote_id'] = loteID;
    }
    if (dataNascimento != null) {
      body["data_nascimento"] = dataNascimento.toUtc().toIso8601String();
    }
    if (producao != null) {
      body["producao"] = producao;
    }
    if (status != null) {
      body["status"] = status;
    }

    http.Response response = await http.put(
      "${env.API_BASE_URL}/animais/$animalID",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
      body: jsonEncode(body),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return response.body.contains("1");
    } else if (response.statusCode == 422) {
      if (response.body.contains("Duplicate entry")) {
        if (response.body.contains("numero_chip")) {
          throw ServiceHandableException(
              'Já existe um animal cadastrado com esse Número de Chip');
        } else if (response.body.contains("numero_brinco")) {
          throw ServiceHandableException(
              'Já existe um animal cadastrado com esse Número de Brinco');
        }
      }
      throw ServiceHandableException('Ocorreu um erro.');
    } else {
      print("Erro ${response.statusCode}\n${response.body}");
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }
}
