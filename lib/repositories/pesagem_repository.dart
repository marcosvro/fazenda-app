import 'dart:convert';

import 'package:app_fazenda/models/pesagem.dart';
import 'package:app_fazenda/models/token.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:app_fazenda/environments/environment.dev.dart' as env;

class PesagemRepository {
  Future<Pesagem> createPesagem({
    @required int fazendaID,
    @required AccessToken token,
  }) async {
    Map<String, dynamic> body = {
      "fazenda_id": fazendaID,
    };

    http.Response response = await http.post(
      "${env.API_BASE_URL}/pesagens",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
      body: jsonEncode(body),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return Pesagem.fromJson(response.body);
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<bool> finalizaPesagem({
    @required int pesagemID,
    @required AccessToken token,
  }) async {
    Map<String, dynamic> body = {
      "finalizada": true,
    };

    http.Response response = await http.put(
      "${env.API_BASE_URL}/pesagens/$pesagemID",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
      body: jsonEncode(body),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return true;
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<List<Pesagem>> getPesagens({
    @required AccessToken token,
    int page,
    int pageSize,
  }) async {
    List<String> queryParams = [];
    if (page != null) queryParams.add("pagina=$page");
    if (pageSize != null) queryParams.add("limite=$pageSize");

    var url =
        "${env.API_BASE_URL}/pesagens${queryParams.length != 0 ? '?' + queryParams.join('&') : ''}";

    http.Response response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
    );

    print(response.body);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      List mapList = json.decode(response.body);
      return mapList.map<Pesagem>((e) => Pesagem.fromMap(e)).toList();
    } else if (response.statusCode == 401) {
      throw ServiceHandableException('TOKEN_EXPIRED');
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  // PESAGEM INDIVIDUAL ---------------------------------------
  Future<PesagemIndividual> createPesagemIndividual({
    @required int fazendaID,
    @required int pesagemID,
    @required int vacaID,
    @required double peso,
    @required AccessToken token,
  }) async {
    Map<String, dynamic> body = {
      "fazenda_id": fazendaID,
      "vaca_id": vacaID,
      "peso": peso,
    };

    http.Response response = await http.post(
      "${env.API_BASE_URL}/pesagens/$pesagemID/individual",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
      body: jsonEncode(body),
    );

    print(response.body);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return PesagemIndividual.fromJson(response.body);
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<List<PesagemIndividual>> getPesagensIndividuais({
    @required AccessToken token,
    @required int pesagemID,
    int page,
    int pageSize,
  }) async {
    List<String> queryParams = [];
    if (page != null) queryParams.add("pagina=$page");
    if (pageSize != null) queryParams.add("limite=$pageSize");

    var url =
        "${env.API_BASE_URL}/pesagens/$pesagemID/individual${queryParams.length != 0 ? '?' + queryParams.join('&') : ''}";

    http.Response response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      List mapList = json.decode(response.body);
      return mapList
          .map<PesagemIndividual>((e) => PesagemIndividual.fromMap(e))
          .toList();
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }

  Future<Pesagem> getLastPesagem({
    @required AccessToken token,
  }) async {
    var url = "${env.API_BASE_URL}/pesagens/last";

    http.Response response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.accessToken}',
      },
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return Pesagem.fromJson(response.body);
    } else if (response.statusCode == 401) {
      throw ServiceHandableException('TOKEN_EXPIRED');
    } else {
      throw ServiceHandableException('Ocorreu um erro.');
    }
  }
}
