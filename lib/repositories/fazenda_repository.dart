import 'dart:convert';

import 'package:app_fazenda/models/fazenda.dart';
import 'package:app_fazenda/environments/environment.dev.dart' as env;
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:http/http.dart' as http;

class FazendaRepository {
  Future<List<Fazenda>> getFazendas({String query}) async {
    http.Response response = await http.get(
      "${env.API_BASE_URL}/fazendas",
      headers: {
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      List mapList = json.decode(response.body);
      return mapList.map<Fazenda>((m) => Fazenda.fromMap(m)).toList();
    } else {
      throw ServiceHandableException(
          'Serviço temporariamente indisponível.. tente novamente mais tarde.');
    }
  }
}
