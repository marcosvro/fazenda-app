import 'package:app_fazenda/environments/environment.dev.dart' as env;
import 'package:app_fazenda/models/token.dart';
import 'package:app_fazenda/models/user.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AuthRepository {
  final String ENDPOINT_SIGN_IN = "${env.API_BASE_URL}/auth/signin";
  final String ENDPOINT_SIGN_UP = "${env.API_BASE_URL}/auth/signup";
  final String ENDPOINT_ACCESS_TOKEN = "${env.API_BASE_URL}/auth/accessToken";

  Future<AccessToken> getAccessToken(String refreshToken, int idFazenda) async {
    http.Response response = await http.post(
      "$ENDPOINT_ACCESS_TOKEN",
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        "fazenda_id": idFazenda,
        "refresh_token": refreshToken,
      }),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return AccessToken.fromMap(json.decode(response.body));
    } else {
      throw ServiceHandableException(
          'Falha ao obter acesso a esta fazenda, certifique-se de que possui tal acesso.');
    }
  }

  Future<RefreshToken> getRefreshToken(String email, String password) async {
    http.Response response = await http.post(
      "$ENDPOINT_SIGN_IN",
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        "email": email,
        "password": password,
      }),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return RefreshToken.fromMap(json.decode(response.body));
    } else if (response.statusCode == 400) {
      throw ServiceHandableException(
          'Credenciais incorretas, informe os dados corretos para logar.');
    } else if (response.statusCode == 422) {
      throw ServiceHandableException('Email ou senha inválido\'s.');
    } else {
      throw ServiceHandableException('Falha ao logar.');
    }
  }

  Future<User> createUser(String email, String password, String username,
      String inviteToken) async {
    http.Response response = await http.post(
      "$ENDPOINT_SIGN_UP",
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        "nome": username,
        "senha": password,
        "email": email,
        "token_acesso": inviteToken
      }),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return User.fromMap(json.decode(response.body));
    } else if (response.statusCode == 400) {
      throw ServiceHandableException(
          'Dados de usuário inválidos. Certifique-se se já não posssui uma conta com este dados ou foneça dados autênticos para o cadastro.');
    } else if (response.statusCode == 422) {
      throw ServiceHandableException(
          'Campos inválidos. Cheque os campos para o cadastro');
    } else {
      throw ServiceHandableException(
          'Serviço temporariamente indisponível.. Tente novamente mais tarde.');
    }
  }
}
