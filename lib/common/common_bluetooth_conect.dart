import 'dart:async';

import 'package:LFReader2/LFReader2.dart';
import 'package:app_fazenda/blocs/bloc_leitora.dart';
import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/providers/leitora.provider.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:provider/provider.dart';

class BluetoothDeviceWidget extends StatefulWidget {
  /// If true, discovery starts on page start, otherwise user must press action button.
  final bool start;
  final bool autoScan;
  final BuildContext parentContext;
  final Function(Tag) onTagReceived;

  const BluetoothDeviceWidget(
      {Key key,
      this.start = true,
      this.autoScan = true,
      @required this.parentContext,
      @required this.onTagReceived})
      : super(key: key);

  @override
  _BluetoothDeviceWidgetState createState() => _BluetoothDeviceWidgetState();
}

class _BluetoothDeviceWidgetState extends State<BluetoothDeviceWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  StreamSubscription<BluetoothDiscoveryResult> _streamSubscription;
  List<BluetoothDiscoveryResult> results = List<BluetoothDiscoveryResult>();
  BluetoothDevice connectedDevice;
  bool isDiscovering;
  bool isConnecting = false;
  bool isReadingTag = false;

  _BluetoothDeviceWidgetState();

  @override
  void initState() {
    super.initState();

    FlutterBluetoothSerial.instance.onStateChanged().listen((event) {
      print(event);
    });

    LeitoraProvider leitoraProvider =
        Provider.of<LeitoraProvider>(widget.parentContext, listen: false);
    connectedDevice = leitoraProvider.bloc.dispositivo;

    if (connectedDevice != null && widget.autoScan) {
      Future.delayed(Duration.zero, () {
        _startReadingTag();
      });
    }

    isDiscovering = widget.start;
    if (isDiscovering) {
      _startDiscovery();
    }
  }

  void _restartDiscovery() {
    setState(() {
      results.clear();
      isDiscovering = true;
    });

    _startDiscovery();
  }

  void _startDiscovery() {
    _streamSubscription =
        FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
      if (!results.any((element) => element.device.address == r.device.address))
        setState(() {
          results.add(r);
        });
    });

    _streamSubscription.onDone(() {
      setState(() {
        isDiscovering = false;
      });
    });
  }

  // @TODO . One day there should be `_pairDevice` on long tap on something... ;)

  @override
  void dispose() {
    // Avoid memory leak (`setState` after dispose) and cancel discovery
    _streamSubscription?.cancel();

    super.dispose();
  }

  void _connectDevice(BluetoothDevice device, BuildContext context) async {
    if (isConnecting) return;
    setState(() {
      isConnecting = true;
    });

    LeitoraProvider leitoraProvider =
        Provider.of<LeitoraProvider>(widget.parentContext, listen: false);
    var connected = await leitoraProvider.bloc.connectLeitora(device);
    if (connected) {
      setState(() {
        connectedDevice = leitoraProvider.bloc.dispositivo;
        if (widget.autoScan) _startReadingTag(ignoreConnect: true);
      });
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          "Não foi possível conectar ao dispositivo.",
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }

    setState(() {
      isConnecting = false;
    });
  }

  void _startReadingTag({ignoreConnect = false}) async {
    if (isReadingTag) return;

    setState(() {
      isReadingTag = true;
    });
    LeitoraProvider leitoraProvider =
        Provider.of<LeitoraProvider>(widget.parentContext, listen: false);
    Tag tag = await leitoraProvider.bloc.scanTag(ignoreConnect: ignoreConnect);

    if (tag != null) {
      widget.onTagReceived(tag);
    } else {
      setState(() {
        connectedDevice = leitoraProvider.bloc.dispositivo;
      });
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          "Falha na leitura do chip.",
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }

    setState(() {
      isReadingTag = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: isDiscovering
            ? Text('Buscando\nDispositivos')
            : Text('Dispositivos\nEncontrados'),
        actions: <Widget>[
          isDiscovering
              ? FittedBox(
                  child: Container(
                    margin: new EdgeInsets.all(16.0),
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    ),
                  ),
                )
              : IconButton(
                  icon: Icon(Icons.replay),
                  onPressed: _restartDiscovery,
                )
        ],
      ),
      body: connectedDevice != null
          ? Center(
              child: isReadingTag
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(
                          Icons.wifi,
                          size: 40,
                        ),
                        Text(
                          "Lendo Chip..\nAproxime o leitor do local do local de aplicação",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15,
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    )
                  : Container(
                      height: 30,
                      width: 150,
                      child: PillGradientButton(
                        "Ler Chip",
                        () => _startReadingTag(),
                      ),
                    ),
            )
          : ListView.builder(
              itemCount: results.length,
              itemBuilder: (BuildContext context, index) {
                BluetoothDiscoveryResult result = results[index];
                return BluetoothDeviceListEntry(
                  device: result.device,
                  rssi: result.rssi,
                  onTap: () {
                    _connectDevice(result.device, context);
                  },
                  onLongPress: () async {
                    try {
                      bool bonded = false;
                      if (result.device.isBonded) {
                        print('Unbonding from ${result.device.address}...');
                        await FlutterBluetoothSerial.instance
                            .removeDeviceBondWithAddress(result.device.address);
                        print(
                            'Unbonding from ${result.device.address} has succed');
                      } else {
                        print('Bonding with ${result.device.address}...');
                        bonded = await FlutterBluetoothSerial.instance
                            .bondDeviceAtAddress(result.device.address);
                        print(
                            'Bonding with ${result.device.address} has ${bonded ? 'succed' : 'failed'}.');
                      }
                      setState(() {
                        results[results.indexOf(result)] =
                            BluetoothDiscoveryResult(
                                device: BluetoothDevice(
                                  name: result.device.name ?? '',
                                  address: result.device.address,
                                  type: result.device.type,
                                  bondState: bonded
                                      ? BluetoothBondState.bonded
                                      : BluetoothBondState.none,
                                ),
                                rssi: result.rssi);
                      });
                    } catch (ex) {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text('Error occured while bonding'),
                            content: Text("${ex.toString()}"),
                            actions: <Widget>[
                              new FlatButton(
                                child: new Text("Close"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    }
                  },
                );
              },
            ),
    );
  }
}

class BluetoothDeviceListEntry extends ListTile {
  BluetoothDeviceListEntry({
    @required BluetoothDevice device,
    int rssi,
    GestureTapCallback onTap,
    GestureLongPressCallback onLongPress,
    bool enabled = true,
  }) : super(
          onTap: onTap,
          onLongPress: onLongPress,
          enabled: enabled,
          leading:
              Icon(Icons.devices), // @TODO . !BluetoothClass! class aware icon
          title: Text(device.name ?? "Unknown device"),
          subtitle: Text(device.address.toString()),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              rssi != null
                  ? Container(
                      margin: new EdgeInsets.all(8.0),
                      child: DefaultTextStyle(
                        style: _computeTextStyle(rssi),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(rssi.toString()),
                            Text('dBm'),
                          ],
                        ),
                      ),
                    )
                  : Container(width: 0, height: 0),
              device.isConnected
                  ? Icon(Icons.import_export)
                  : Container(width: 0, height: 0),
              device.isBonded
                  ? Icon(Icons.link)
                  : Container(width: 0, height: 0),
            ],
          ),
        );

  static TextStyle _computeTextStyle(int rssi) {
    /**/ if (rssi >= -35)
      return TextStyle(color: Colors.greenAccent[700]);
    else if (rssi >= -45)
      return TextStyle(
          color: Color.lerp(
              Colors.greenAccent[700], Colors.lightGreen, -(rssi + 35) / 10));
    else if (rssi >= -55)
      return TextStyle(
          color: Color.lerp(
              Colors.lightGreen, Colors.lime[600], -(rssi + 45) / 10));
    else if (rssi >= -65)
      return TextStyle(
          color: Color.lerp(Colors.lime[600], Colors.amber, -(rssi + 55) / 10));
    else if (rssi >= -75)
      return TextStyle(
          color: Color.lerp(
              Colors.amber, Colors.deepOrangeAccent, -(rssi + 65) / 10));
    else if (rssi >= -85)
      return TextStyle(
          color: Color.lerp(
              Colors.deepOrangeAccent, Colors.redAccent, -(rssi + 75) / 10));
    else
      /*code symetry*/
      return TextStyle(color: Colors.redAccent);
  }
}
