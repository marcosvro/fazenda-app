import 'package:LFReader2/LFReader2.dart';
import 'package:app_fazenda/common/common_bluetooth_conect.dart';
import 'package:app_fazenda/common/common_buttons.dart';
import 'package:app_fazenda/models/animal.dart';
import 'package:app_fazenda/providers/auth.provider.dart';
import 'package:app_fazenda/repositories/animal_repository.dart';
import 'package:app_fazenda/utils/helpers/api_response.dart';
import 'package:app_fazenda/utils/helpers/exceptions.dart';
import 'package:app_fazenda/utils/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:provider/provider.dart';

class LeitoraButtton extends StatefulWidget {
  final Function(Animal) onAnimalReceived;

  LeitoraButtton({Key key, @required this.onAnimalReceived}) : super(key: key);

  @override
  _LeitoraButttonState createState() => _LeitoraButttonState();
}

class _LeitoraButttonState extends State<LeitoraButtton> {
  bool isFindingAnimal = false;
  AnimalRepository animalRepository = AnimalRepository();

  void _findAnimalByNumeroDoChip(Tag tag) async {
    if (tag == null || tag.serial == "" || isFindingAnimal) return;

    setState(() {
      isFindingAnimal = true;
    });

    String error;
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    try {
      var animais = await animalRepository.getAnimais(
        fazendaID: authProvider.bloc.fazendaObject?.id,
        token: authProvider.bloc.accessTokenObject,
        queryKey: "numero_chip",
        queryValue: tag.serial,
      );

      print("Animais recebido com a tag : $animais");

      if (animais != null && animais.length > 0) {
        widget.onAnimalReceived(animais[0]);
      } else {
        openCadastrarBrincoScreen(context, tag);
      }
    } catch (e) {
      print(e);
      if (e.runtimeType == ServiceHandableException) {
        ServiceHandableException exception = (e as ServiceHandableException);
        error = exception.message;
        if (exception.message.contains("TOKEN_EXPIRED")) {
          error = "Token expirado faça login.. por favor faça login novamente.";
          authProvider.bloc.changeAccessToken(ApiResponse.error(""));
        }
      } else {
        error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
      }
    }

    if (error != null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }

    setState(() {
      isFindingAnimal = false;
    });
  }

  void openBluetoohDeviceScreen(BuildContext ctx) {
    if (isFindingAnimal) return;
    showDialog(
        context: ctx,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(0),
            content: BluetoothDeviceWidget(
              parentContext: ctx,
              onTagReceived: (tag) {
                Navigator.of(context).pop();
                _findAnimalByNumeroDoChip(tag);
              },
            ),
          );
        });
  }

  void openCadastrarBrincoScreen(BuildContext ctx, Tag tag) {
    showDialog(
        context: ctx,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(0),
            content: CadastrarBrinco(
              tag: tag,
              onAnimalUpdated: (animal) {
                Navigator.of(context).pop();
                widget.onAnimalReceived(animal);
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: PRIMARY_SWATCH)),
      color: Colors.transparent,
      textColor: PRIMARY_SWATCH,
      padding: EdgeInsets.all(8.0),
      onPressed: () => openBluetoohDeviceScreen(context),
      child: isFindingAnimal
          ? SizedBox(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(PRIMARY_SWATCH),
              ),
              width: 14,
              height: 14,
            )
          : Icon(Icons.wifi),
    );
  }
}

class CadastrarBrinco extends StatefulWidget {
  final Tag tag;
  final Function(Animal) onAnimalUpdated;

  CadastrarBrinco({@required this.tag, @required this.onAnimalUpdated});

  @override
  _CadastrarBrincoState createState() => _CadastrarBrincoState();
}

class _CadastrarBrincoState extends State<CadastrarBrinco> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _typeAheadController = TextEditingController();
  AnimalRepository animalRepository = AnimalRepository();
  Animal _selectedAnimal;
  bool _isLoading = false;

  Future<List<Animal>> _getAnimais(String numeroDoBrinco) async {
    try {
      AuthProvider authProvider =
          Provider.of<AuthProvider>(context, listen: false);
      var animais = await animalRepository.getAnimais(
        fazendaID: authProvider.bloc.fazendaObject?.id,
        token: authProvider.bloc.accessTokenObject,
        queryKey: "numero_brinco",
        queryValue: numeroDoBrinco,
      );
      print(animais);
      return animais;
    } catch (e) {
      print(e);
      return <Animal>[];
    }
  }

  _updateAnimal() async {
    if (_isLoading) return;
    setState(() {
      _isLoading = true;
    });
    String error;
    if (_selectedAnimal.id != null && _selectedAnimal.id != 0) {
      AuthProvider authProvider =
          Provider.of<AuthProvider>(context, listen: false);
      try {
        var updated = await animalRepository.updateAnimal(
          token: authProvider.bloc.accessTokenObject,
          animalID: _selectedAnimal.id,
          numeroChip: widget.tag.serial,
        );
        print("Retorno da request ${updated}");

        if (updated) {
          _selectedAnimal.numeroChip = widget.tag.serial;
          print("Animal Atualizado!");
          widget.onAnimalUpdated(_selectedAnimal);
        }
      } catch (e) {
        print(e);
        if (e.runtimeType == ServiceHandableException) {
          ServiceHandableException exception = (e as ServiceHandableException);
          error = exception.message;
          if (exception.message.contains("TOKEN_EXPIRED")) {
            error =
                "Token expirado faça login.. por favor faça login novamente.";
            authProvider.bloc.changeAccessToken(ApiResponse.error(""));
          }
        } else {
          error = "Ocorreu um erro desconhecido.. tente novamente mais tarde.";
        }
      }
    } else {
      error = "Precisa selecionar um animal para cadastrar o chip.";
    }
    if (error != null) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          error,
          style: TextStyle(color: Colors.redAccent),
        ),
        behavior: SnackBarBehavior.floating,
      ));
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("ATENÇÃO"),
            SizedBox(height: 10),
            Text(
              "Este chip não está vinculado a nenhum animal em nosso sistemas. Informe o número do brinco do animal abaixo para cadastrar o chip.",
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30),
              child: TypeAheadFormField<Animal>(
                textFieldConfiguration: TextFieldConfiguration(
                    controller: this._typeAheadController,
                    decoration: InputDecoration(labelText: 'Animal')),
                suggestionsCallback: _getAnimais,
                itemBuilder: (context, suggestion) {
                  return ListTile(
                    title: Text('#${suggestion.numeroBrinco}'),
                  );
                },
                transitionBuilder: (context, suggestionsBox, controller) {
                  return suggestionsBox;
                },
                onSuggestionSelected: (suggestion) {
                  this._typeAheadController.text = '${suggestion.numeroBrinco}';
                  this._selectedAnimal = suggestion;
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Por favor, selecione um animal';
                  }
                  return null;
                },
                onSaved: (value) => _updateAnimal(),
              ),
            ),
            SizedBox(
              height: 42,
              width: 230,
              child: PillGradientButton(
                "CADASTRAR",
                _updateAnimal,
                loading: _isLoading,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
