import 'package:app_fazenda/utils/theme/colors.dart' as colors;
import 'package:flutter/material.dart';

class PillGradientButton extends StatelessWidget {
  final String text;
  final Function action;
  final double padding;
  final bool loading;
  final double fontSize;
  final Color bgColor;
  final Color textColor;
  final IconData icon;

  PillGradientButton(this.text, this.action,
      {this.padding = 0,
      this.loading = false,
      this.fontSize = 14,
      this.bgColor,
      this.textColor = Colors.white,
      this.icon});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      clipBehavior: Clip.antiAlias,
      onPressed: loading ? null : action,
      textColor: textColor,
      padding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Container(
        decoration: BoxDecoration(
          gradient: colors.PRIMARY_GRADIENT_HORIZONTAL,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: padding,
          vertical: 0,
        ),
        child: Center(
          child: loading
              ? SizedBox(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(textColor),
                  ),
                  width: fontSize,
                  height: fontSize,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    if (icon != null)
                      Icon(
                        icon,
                        color: textColor,
                        size: fontSize * 1.8,
                      ),
                    Text(
                      text,
                      style: TextStyle(
                        fontSize: fontSize,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}

class PillOutlineButton extends StatelessWidget {
  final String text;
  final Function action;
  final Color color;
  final Color bgColor;
  final double paddingHori;
  final double paddingVert;
  final double fontSize;
  final bool loading;
  final IconData icon;

  const PillOutlineButton(this.text, this.action,
      {this.loading = false,
      this.color,
      this.bgColor,
      this.paddingHori,
      this.paddingVert,
      this.fontSize,
      this.icon});

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      color: color ?? Theme.of(context).textTheme.button.color,
      onPressed: loading ? null : action,
      padding: EdgeInsets.all(0),
      child: loading
          ? SizedBox(
              child: CircularProgressIndicator(
                valueColor:
                    AlwaysStoppedAnimation<Color>(color ?? Colors.white),
              ),
              width: fontSize ?? 18,
              height: fontSize ?? 18,
            )
          : Container(
              decoration: BoxDecoration(color: bgColor ?? this.bgColor),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: paddingHori ?? 0, vertical: paddingVert ?? 0),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.min,
                    //textBaseline: TextBaseline.alphabetic,
                    children: <Widget>[
                      if (icon != null)
                        Icon(
                          icon,
                          color:
                              color ?? Theme.of(context).textTheme.button.color,
                          size: fontSize == null ? null : fontSize * 1.8,
                        ),
                      Text(
                        text,
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: fontSize == null ? null : fontSize,
                          letterSpacing: 1.5,
                          color:
                              color ?? Theme.of(context).textTheme.button.color,
                        ),
                        overflow: TextOverflow.fade,
                      ),
                    ],
                  ),
                ),
              ),
            ),
      borderSide: BorderSide(
        color: color ?? Theme.of(context).textTheme.button.color,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
    );
  }
}
