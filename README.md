<p align="center">
  <img src="/assets/flutter.png" width="150">
</p>

<h2 align="center">Farm Helper Flutter App</h2>
<p align="center">A farm Flutter app to help employees register animals, carry out collection weighing and categorize animals</p>

<h2>This application uses concepts such as</h2>
<ul>
  <li>Reactive programming (Bloc)</li>
  <li>Bluetooth device connection</li>
  <li>Communication with APIs</li>
  <li>Authentication/Authorization</li>
  <li>Multiple style themes</li>
  <li>Custom library (Android) for connection with chip reader device</li>
</ul>

<h2>Printscreens</h2>
<table>
  <tr>
    <td><span><img src="/assets/login.jpeg" width="350"></span></td>
    <td><span><img src="/assets/select-farm.jpeg" width="350"></span></td>
    <td><span><img src="/assets/register-batch.jpeg" width="350"></span></td>
    <td><span><img src="/assets/register-animal.jpeg" width="350"></span></td>
  </tr>
  <tr>
    <td>Login Page</td>
    <td>Select Farm Page</td>
    <td>Register batch</td>
    <td>Register animals</td>
  </tr>
</table>

<table>
  <tr>
    <td><span><img src="/assets/animals-list.jpeg" width="350"></span></td>
    <td><span><img src="/assets/search.jpeg" width="350"></span></td>
    <td><span><img src="/assets/weighing-open.jpeg" width="350"></span></td>
    <td><span><img src="/assets/weighing-close.jpeg" width="350"></span></td>
  </tr>
  <tr>
    <td>Animals listr</td>
    <td>Search filters</td>
    <td>Weighing Open</td>
    <td>Weighing Close</td>
  </tr>
</table>

<table>
  <tr>
    <td><span><img src="/assets/weighing-list.jpeg" width="350"></span></td>
    <td><span><img src="/assets/classification.jpeg" width="350"></span></td>
    <td><span><img src="/assets/dark-mode-weighing.jpeg" width="350"></span></td>
    <td><span><img src="/assets/dark-mode-classification.jpeg" width="350"></span></td>
  </tr>
  <tr>
    <td>Weighing List Page</td>
    <td>Classification Page</td>
    <td>Dark Mode Weighing List Page</td>
    <td>Dark Mode Classification Page</td>
  </tr>
</table>

<h2>Clone development setup</h2>

> #### Required
>
> 1. Flutter Setup configured (Follow the Flutter Docs or search on Google "how to configure Flutter on [Your operational System]")

#### Steps

1. Clone repository
2. Open repository on VSCode
3. Configure enviroment variables
4. Press F5 to start debug version
5. Enjoy

<h2>Generate Build</h2>

No secrets, only follow the [Flutter Docs Steps](https://flutter.dev/docs/deployment/android) and everything is okay

<br>
<br>
<br>