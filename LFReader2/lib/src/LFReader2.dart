part of LFReader2;

class LFReader2 {
  static const MethodChannel _channel =
      const MethodChannel("$NAMESPACE/methods");
  final EventChannel _tagChannel = const EventChannel('$NAMESPACE/state');
  final EventChannel _connectionChannel =
      const EventChannel('$NAMESPACE/connection');
  final StreamController<MethodCall> _methodStreamController =
      new StreamController.broadcast(); // ignore: close_sinks

  /// Singleton boilerplate
  LFReader2._() {
    _channel.setMethodCallHandler((MethodCall call) {
      _methodStreamController.add(call);
      return;
    });
  }
  static LFReader2 _instance = new LFReader2._();
  static LFReader2 get instance => _instance;

  /// Gets the current state of the Bluetooth module
  Stream<Tag> get tagStream async* {
    yield* _tagChannel.receiveBroadcastStream().map((buffer) {
      return Tag.fromMap(buffer);
    });
  }

  /// Gets the current state of the Bluetooth connection
  Stream<LFReader2ConnectionState> get connectionStream async* {
    yield* _connectionChannel.receiveBroadcastStream().map((buffer) {
      return LFReader2ConnectionState.fromUnderlyingValue(buffer);
    });
  }

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  /// Conect to a device with ID [macId]
  Future connectDevice(String macId) async {
    await _channel.invokeMethod("connectDevice", macId);
  }

  /// Conect to a device with ID [macId]
  Future startRead() async {
    await _channel.invokeMethod("startRead");
  }

  Future cmd() async {
    Uint8List resp = await _channel.invokeMethod("cmd");
    print(resp);
  }
}
