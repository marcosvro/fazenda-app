part of LFReader2;

class Tag {
  Tag({
    this.type,
    this.serial,
  });

  String type;
  String serial;

  factory Tag.fromJson(String str) => Tag.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Tag.fromMap(Map<dynamic, dynamic> json) => Tag(
        type: json != null ? json["type"] : "",
        serial: json != null ? json["serial"] : "",
      );

  Map<String, dynamic> toMap() => {
        "type": type,
        "serial": serial,
      };
}
