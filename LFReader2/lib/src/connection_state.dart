part of LFReader2;

class LFReader2ConnectionState {
  final int underlyingValue;
  final String stringValue;

  const LFReader2ConnectionState.fromString(String string)
      : this.underlyingValue = (string == 'CONNECTED'
                ? 10
                : string == 'DISCONNECTED'
                    ? 11
                    : string == 'ERROR'
                        ? -1
                        : -2 // Unknown, if not found valid
            ),
        this.stringValue = ((string == 'CONNECTED' ||
                    string == 'DISCONNECTED' ||
                    string == 'ERROR')
                ? string
                : 'UNKNOWN' // Unknown, if not found valid
            );

  const LFReader2ConnectionState.fromUnderlyingValue(int value)
      : this.underlyingValue = (((value >= 10 && value <= 16) || value == -1)
                ? value
                : -2 // Unknown, if not found valid
            ),
        this.stringValue = (value == 10
                ? 'CONNECTED'
                : value == 11
                    ? 'DISCONNECTED'
                    : value == -1
                        ? 'ERROR'
                        : 'UNKNOWN' // Unknown, if not found valid
            );

  String toString() => 'LFReader2ConnectionState.$stringValue';

  int toUnderlyingValue() => underlyingValue;

  static const CONNECTED = LFReader2ConnectionState.fromUnderlyingValue(10);
  static const DISCONNECTED = LFReader2ConnectionState.fromUnderlyingValue(11);

  static const ERROR = LFReader2ConnectionState.fromUnderlyingValue(-1);
  static const UNKNOWN = LFReader2ConnectionState.fromUnderlyingValue(-2);

  operator ==(Object other) {
    return other is LFReader2ConnectionState &&
        other.underlyingValue == this.underlyingValue;
  }

  @override
  int get hashCode => underlyingValue.hashCode;

  bool get isConnected => this == CONNECTED;
}
