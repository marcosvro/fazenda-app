package com.example.LFReader2;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.TargetApi;
import android.Manifest;
import android.app.Activity;
import android.app.Application;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.EventChannel.StreamHandler;
import io.flutter.plugin.common.EventChannel.EventSink;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.BinaryMessenger;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.Tag;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.ParcelUuid;
import android.os.Message;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Array;
import java.util.Arrays;
import java.util.UUID;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
//import com.zrdz.lfreader.LFReader;



/** LFReader2Plugin */
public class LFReader2Plugin implements FlutterPlugin, ActivityAware, MethodCallHandler, RequestPermissionsResultListener, StreamHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private static LFReader2Plugin instance;
  private static final int REQUEST_FINE_LOCATION_PERMISSIONS = 1452;
  private FlutterPluginBinding pluginBinding;
  private ActivityPluginBinding activityBinding;
  private Object initializationLock = new Object();
  private static final String TAG = "FlutterLFReaderPluggin";
  private Activity activity;
  private Application application;
  private static final String NAMESPACE = "plugins.example.com/LFReader2";
  private EventChannel stateChannel;
  private EventChannel connectionChannel;
  private StreamHandler connectionStreamHandler;
  private EventSink connectionSink;
  private BluetoothManager mBluetoothManager;
  private BluetoothAdapter mBluetoothAdapter;
  private Result pendingResult;
  private MethodCall pendingCall;
  private boolean allowDuplicates = false;
  private ArrayList<String> macDeviceScanned = new ArrayList<>();
  private ScanCallback scanCallback21;
  private BluetoothAdapter.LeScanCallback scanCallback18;
  private LFReader mLFReader;
  private Context context;


  private void setup(
    final BinaryMessenger messenger,
    final Application application,
    final Activity activity,
    final Registrar registrar,
    final ActivityPluginBinding activityBinding) {
    synchronized (initializationLock) {
      Log.i(TAG, "setup");
      this.activity = activity;
      this.application = application;
      channel = new MethodChannel(messenger, NAMESPACE + "/methods");
      channel.setMethodCallHandler(this);
      stateChannel = new EventChannel(messenger, NAMESPACE + "/state");
      stateChannel.setStreamHandler(this);
      connectionChannel = new EventChannel(messenger, NAMESPACE + "/connection");
      connectionStreamHandler = new StreamHandler() {
        @Override
        public void onListen(Object o, EventSink eventSink) {
            connectionSink = eventSink;
        }
        @Override
        public void onCancel(Object o) {
            if (connectionSink != null) {
                connectionSink.endOfStream();
                connectionSink = null;
            }
        }
    };
      connectionChannel.setStreamHandler(connectionStreamHandler);
      mBluetoothManager = (BluetoothManager) application.getSystemService(Context.BLUETOOTH_SERVICE);
      mBluetoothAdapter = mBluetoothManager.getAdapter();
      mLFReader = LFReader.getInstance(mHandler);
      if (registrar != null) {
          // V1 embedding setup for activity listeners.
          registrar.addRequestPermissionsResultListener(this);
      } else  if (activityBinding != null){
          // V2 embedding setup for activity listeners.
          activityBinding.addRequestPermissionsResultListener(this);
      }
    }
  }

  private void tearDown() {
    Log.i(TAG, "teardown");
    activityBinding.removeRequestPermissionsResultListener(this);
    activityBinding = null;
    channel.setMethodCallHandler(null);
    channel = null;
    stateChannel.setStreamHandler(null);
    stateChannel = null;
    mBluetoothAdapter = null;
    mBluetoothManager = null;
    application = null;
  }

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    pluginBinding = flutterPluginBinding;
  }

  @Override
  public void onDetachedFromEngine(FlutterPluginBinding binding) {
    pluginBinding = null;
    tearDown();
  }

  @Override
  public void onAttachedToActivity(ActivityPluginBinding binding) {
    activityBinding = binding;
    setup(
      pluginBinding.getBinaryMessenger(),
      (Application) pluginBinding.getApplicationContext(),
      activityBinding.getActivity(),
      null,
      activityBinding
    );
  }

  @Override
  public void onDetachedFromActivity() {
      tearDown();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    onDetachedFromActivity();
  }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {
    onAttachedToActivity(binding);
  }

  public static void registerWith(Registrar registrar) {
    Activity activity = registrar.activity();
    Application application = null;
    if (registrar.context() != null) {
      application = (Application) (registrar.context().getApplicationContext());
    }
    if (instance == null) {
      instance = new LFReader2Plugin();
    }
    // final MethodChannel channel = new MethodChannel(registrar.messenger(), "LFReader2");
    // channel.setMethodCallHandler(new LFReader2Plugin());
    instance.setup(registrar.messenger(), application, activity, registrar, null);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    Log.i(TAG,"Nova chamada :" + call.method);
    if(mBluetoothAdapter == null && "isAvailable" != call.method) {
      result.error("bluetooth_unavailable", "the device does not have bluetooth", null);
      return;
    }

    switch (call.method) {
      case "cmd": {
        byte[] resp = mLFReader.sendCommandGetResponse(new byte[]{-2, 0, 12, -2, 1});
        if (resp != null && resp.length > 0 && resp[0] == (byte)-115)
          Log.i(TAG, "Beep concluido!");
        result.success(resp);
        break;
      }
      case "connectDevice": {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            activity,
                            new String[] {
                                    Manifest.permission.ACCESS_FINE_LOCATION
                            },
                            REQUEST_FINE_LOCATION_PERMISSIONS);
                    pendingCall = call;
                    pendingResult = result;
                    break;
                }
        String adresss = (String)call.arguments;
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(adresss);
        try {
          mLFReader.connect(device, pluginBinding.getApplicationContext());
        } catch (Exception e) {
          Log.e(TAG,e.getMessage());
          result.error("device_connec_error", e.getMessage(), e);
          break;
        }
        result.success(null);
        break;
      }
      case "startScan": {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          ActivityCompat.requestPermissions(
            activity,
            new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION
            },
            REQUEST_FINE_LOCATION_PERMISSIONS
          );
          pendingCall = call;
          pendingResult = result;
          break;
        }
        startScan(call, result);
        break;
      }
      case "startRead": {
        try {
          mLFReader.startReadTag();
        } catch (Exception e) {
          result.error("device_connec_error", e.getMessage(), e);
          break;
        }
        result.success(null);
        break;
      }
      case "stopScan": {
        stopScan();
        result.success(null);
        break;
      }
    }
  }

  private void startScan(MethodCall call, Result result) {
    byte[] data = call.arguments();
    Protos.ScanSettings settings;
    try {
        settings = Protos.ScanSettings.newBuilder().mergeFrom(data).build();
        allowDuplicates = settings.getAllowDuplicates();
        macDeviceScanned.clear();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startScan21(settings);
        } else {
            startScan18(settings);
        }
        result.success(null);
    } catch (Exception e) {
        result.error("startScan", e.getMessage(), e);
    }
  }

  private void stopScan() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      stopScan21();
    } else {
      stopScan18();
    }
  }

  @TargetApi(21)
  private void startScan21(Protos.ScanSettings proto) throws IllegalStateException {
    BluetoothLeScanner scanner = mBluetoothAdapter.getBluetoothLeScanner();
    if(scanner == null) throw new IllegalStateException("getBluetoothLeScanner() is null. Is the Adapter on?");
    int scanMode = proto.getAndroidScanMode();
    int count = proto.getServiceUuidsCount();
    List<ScanFilter> filters = new ArrayList<>(count);
    for(int i = 0; i < count; i++) {
      String uuid = proto.getServiceUuids(i);
      ScanFilter f = new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(uuid)).build();
      filters.add(f);
    }
    ScanSettings settings = new ScanSettings.Builder().setScanMode(scanMode).build();
    scanner.startScan(filters, settings, getScanCallback21());
  }

  @TargetApi(21)
  private void stopScan21() {
    BluetoothLeScanner scanner = mBluetoothAdapter.getBluetoothLeScanner();
    if(scanner != null) scanner.stopScan(getScanCallback21());
  }

  @TargetApi(21)
  private ScanCallback getScanCallback21() {
    if(scanCallback21 == null){
      scanCallback21 = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
          super.onScanResult(callbackType, result);
          if (!allowDuplicates && result != null && result.getDevice() != null && result.getDevice().getAddress() != null) {
            if (macDeviceScanned.contains(result.getDevice().getAddress())) return;
            macDeviceScanned.add(result.getDevice().getAddress());
          }
          Protos.ScanResult scanResult = ProtoMaker.from(result.getDevice(), result);
          invokeMethodUIThread("ScanResult", scanResult.toByteArray());
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
          super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
          super.onScanFailed(errorCode);
        }
      };
    }
    return scanCallback21;
  }


  private void startScan18(Protos.ScanSettings proto) throws IllegalStateException {
    List<String> serviceUuids = proto.getServiceUuidsList();
    UUID[] uuids = new UUID[serviceUuids.size()];
    for(int i = 0; i < serviceUuids.size(); i++) {
      uuids[i] = UUID.fromString(serviceUuids.get(i));
    }
    boolean success = mBluetoothAdapter.startLeScan(uuids, getScanCallback18());
    if(!success) throw new IllegalStateException("getBluetoothLeScanner() is null. Is the Adapter on?");
  }

  private void stopScan18() {
    mBluetoothAdapter.stopLeScan(getScanCallback18());
  }

  private BluetoothAdapter.LeScanCallback getScanCallback18() {
    if(scanCallback18 == null) {
      scanCallback18 = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice bluetoothDevice, int rssi, byte[] scanRecord) {
          if (!allowDuplicates && bluetoothDevice != null && bluetoothDevice.getAddress() != null) {
            if (macDeviceScanned.contains(bluetoothDevice.getAddress())) return;
            macDeviceScanned.add(bluetoothDevice.getAddress());
          }
          Protos.ScanResult scanResult = ProtoMaker.from(bluetoothDevice, scanRecord, rssi);
          invokeMethodUIThread("ScanResult", scanResult.toByteArray());
        }
      };
    }
    return scanCallback18;
  }


  private EventSink sink;

  private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      final String action = intent.getAction();

      if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
        final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                BluetoothAdapter.ERROR);
        switch (state) {
          case BluetoothAdapter.STATE_OFF:
            sink.success(Protos.BluetoothState.newBuilder().setState(Protos.BluetoothState.State.OFF).build().toByteArray());
            break;
          case BluetoothAdapter.STATE_TURNING_OFF:
            sink.success(Protos.BluetoothState.newBuilder().setState(Protos.BluetoothState.State.TURNING_OFF).build().toByteArray());
            break;
          case BluetoothAdapter.STATE_ON:
            sink.success(Protos.BluetoothState.newBuilder().setState(Protos.BluetoothState.State.ON).build().toByteArray());
            break;
          case BluetoothAdapter.STATE_TURNING_ON:
            sink.success(Protos.BluetoothState.newBuilder().setState(Protos.BluetoothState.State.TURNING_ON).build().toByteArray());
            break;
        }
      }
    }
  };

  @Override
  public void onListen(Object o, EventSink eventSink) {
    sink = eventSink;
    //IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
    //activity.registerReceiver(mReceiver, filter);
  }

  @Override
  public void onCancel(Object o) {
    sink = null;
    //activity.unregisterReceiver(mReceiver);
  }

  public Handler mHandler = new Handler(Looper.getMainLooper()) {
    @Override
    public void handleMessage(Message msg) {
      switch (msg.what) {
        case LFReader.STATE_CONNECTING:
          Log.i(TAG, "Connectando ao dispositivo!");
          break;
        case LFReader.STATE_CONNECTED:
          if (connectionSink != null) {
            connectionSink.success(10);
          }
          Log.i(TAG, "Dispositivo conectado!");
          break;
        case LFReader.STATE_CONNECT_FAIL:
          if (connectionSink != null) {
            connectionSink.success(-1);
          }
          Log.i(TAG, "Falha na conexão com o dispositivo!");
          break;
        case LFReader.STATE_READ_TAG_SUCCESS:
          Log.i(TAG, "Mensagem lida com sucesso");
          Map<String, String> resMap = (Map<String,String>) msg.obj;
          String serial = resMap.get(LFReader.SERIAL);
          String type = resMap.get(LFReader.TYPE);
          Log.i(TAG, "Type: " + type);
          Log.i(TAG, "Serial: " + serial);
          sink.success(resMap);
          break;
        case LFReader.STATE_NO_TAG_FOUND:
          Log.i(TAG, "Falha na leitura da tag!");
          sink.success(null);
          break;
        default:
          Log.i(TAG, "Mensagem não tratada: " +  msg.what);
          break;
      }
    }
  };
  

  @Override
  public boolean onRequestPermissionsResult(
    int requestCode, String[] permissions, int[] grantResults) {
    if (requestCode == REQUEST_FINE_LOCATION_PERMISSIONS) {
      if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        Log.i(TAG, "Permission granted!");
      } else {
        pendingResult.error("no_permissions", "flutter_blue plugin requires location permissions for scanning", null);
        pendingResult = null;
        pendingCall = null;
      }
      return true;
    }
    return false;
  }

  private void invokeMethodUIThread(final String name, final byte[] byteArray)
  {
    activity.runOnUiThread(
            new Runnable() {
                @Override
                public void run() {
                    channel.invokeMethod(name, byteArray);
                }
            });
  }
}
