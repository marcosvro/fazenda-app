package com.example.LFReader2;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.zrdz.lfreader.Transponder;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LFReader {
    private static LFReader mLFReader;
    private BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
    private Handler mHandler;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    public static final byte[] READ_TAG_COMMAND = new byte[]{-2, 0, 8, 15, -2, 1};
    public static final byte[] PING_DEVICE_COMMAND = new byte[]{-2, 0, -2, 1};
    private int mState;
    private boolean read_flag = false;
    private LFReader.ConnectThread mConnectThread;
    private LFReader.ConnectedThread mConnectedThread;
    private InputStream mInputStream;
    private OutputStream mOutputStream;
    public static final String SERIAL = "serial";
    public static final String TYPE = "type";
    public static final int STATE_NONE = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    public static final int STATE_CONNECT_FAIL = 1001;
    public static final int STATE_READ_TAG_FAIL = 1002;
    public static final int STATE_READ_TAG_SUCCESS = 1003;
    public static final int STATE_NO_TAG_FOUND = 1004;

    public LFReader(Handler handler) {
        this.mHandler = handler;
    }

    public static LFReader getInstance(Handler handler) {
        if (mLFReader == null) {
            Class var1 = LFReader.class;
            synchronized(LFReader.class) {
                if (mLFReader == null) {
                    mLFReader = new LFReader(handler);
                }
            }
        }

        return mLFReader;
    }

    public synchronized void connect(final BluetoothDevice device, Context context) {
        if (device.getBondState() != 12) {
            IntentFilter filter = new IntentFilter("android.bluetooth.device.action.PAIRING_REQUEST");
            context.registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                        try {
                            byte[] pin = (byte[])BluetoothDevice.class.getMethod("convertPinToBytes", String.class).invoke(BluetoothDevice.class, "1234");
                            Method m = device.getClass().getMethod("setPin", byte[].class);
                            m.invoke(device, pin);
                            device.getClass().getMethod("setPairingConfirmation", Boolean.TYPE).invoke(device, true);
                            System.out.println("PAIRED !");
                            context.unregisterReceiver(this);
                        } catch (Exception var5) {
                            var5.printStackTrace();
                        }
                    }

                }
            }, filter);
        }

        if (this.mState == 1 && this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }

        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }

        this.mConnectThread = new LFReader.ConnectThread(device, context);
        this.mConnectThread.start();
        this.setState(1);
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device, Context context) {
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }

        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }

        this.mConnectedThread = new LFReader.ConnectedThread(socket, device);
        this.mConnectedThread.start();
        this.setState(2);
        Message msg = this.mHandler.obtainMessage(2);
        this.mHandler.sendMessage(msg);
    }

    private void connectionFailed() {
        this.setState(0);
        Message msg = this.mHandler.obtainMessage(1001);
        this.mHandler.sendMessage(msg);
    }

    private String parseReceiveString(String str) {
        try {
            int start = 0;
            int end = 0;
            int first = 0;
            String serial = null;
            if ((start = str.indexOf("UID: ")) != -1) {
                start += 5;
                if ((end = str.indexOf("   *")) != -1) {
                    first = end + 5;
                    str = str.substring(first);
                }
            }

            if ((start = str.indexOf("   ")) != -1) {
                start += 3;
                if ((end = str.indexOf("*")) != -1) {
                    serial = str.substring(start, end).trim();
                }
            } else {
                serial = "";
            }

            return serial;
        } catch (Exception var6) {
            var6.printStackTrace();
            return null;
        }
    }

    public synchronized void setState(int state) {
        this.mState = state;
    }

    public synchronized int getState() {
        return this.mState;
    }

    public boolean pingDevice() {
        if (this.read_flag) {
            return false;
        } else {
            try {
                Thread.sleep(100L);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }

            byte[] code = this.sendCommandGetResponse(PING_DEVICE_COMMAND);
            return code != null;
        }
    }

    public byte[] sendCommandGetResponse(byte[] array) {
        try {
            int r = 1;

            while(r > 0) {
                if (this.mInputStream != null) {
                    r = this.mInputStream.available();
                    if (r > 0) {
                        byte[] b = new byte[r];
                        r = this.mInputStream.read(b, 0, r);
                    }
                }
            }

            this.write(array);
            int error = 0;

            int count;
            while((count = this.mInputStream.available()) == 0) {
                Thread.sleep(100L);
                ++error;
                if (error > 10) {
                    error = 0;
                    return null;
                }
            }

            byte[] data = new byte[count];
            this.mInputStream.read(data, 0, count);
            return data;
        } catch (IOException var6) {
            var6.printStackTrace();
        } catch (Exception var7) {
            var7.printStackTrace();
        }

        return null;
    }

    private void write(byte[] out) {
        LFReader.ConnectedThread r;
        synchronized(this) {
            if (this.mState != 2) {
                return;
            }

            r = this.mConnectedThread;
        }

        r.write(out);
    }

    public void startReadTag() {
        if (!this.read_flag) {
            try {
                int r = 1;

                while(r > 0) {
                    r = this.mInputStream.available();
                    if (r > 0) {
                        byte[] b = new byte[r];
                        r = this.mInputStream.read(b, 0, r);
                    }
                }
            } catch (IOException var3) {
                var3.printStackTrace();
                return;
            }

            this.read_flag = true;
            (new Thread(new Runnable() {
                public void run() {
                    try {
                        LFReader.this.mOutputStream.write(LFReader.READ_TAG_COMMAND);
                        LFReader.this.mOutputStream.flush();
                    } catch (IOException var11) {
                        var11.printStackTrace();
                        LFReader.this.read_flag = false;
                        LFReader.this.mHandler.sendEmptyMessage(1002);
                        return;
                    }

                    try {
                        DataInputStream dis = new DataInputStream(LFReader.this.mInputStream);
                        int answer = dis.readByte() & 255;
                        if (answer != 136) {
                            if (answer == 126) {
                                LFReader.this.read_flag = false;
                                LFReader.this.mHandler.sendEmptyMessage(1004);
                            } else {
                                LFReader.this.read_flag = false;
                                LFReader.this.mHandler.sendEmptyMessage(1002);
                            }
                        } else {
                            byte[] id = new byte[8];
                            dis.read(id);
                            int country = ((id[1] & 255) << 8) + (id[2] & 255);
                            if (country > 999) {
                                country = 999;
                            }

                            long code = 0L;

                            for(int i = 0; i < 5; ++i) {
                                code <<= 8;
                                code += (long)(id[7 - i] & 255);
                            }

                            String serial = String.format("%03d %012d", country, code);
                            if (!TextUtils.isEmpty(serial)) {
                                String type = Transponder.getTypeString(id[0]);
                                Map<String, String> resultMap = new HashMap();
                                resultMap.put("serial", serial);
                                resultMap.put("type", type);
                                Message msg = Message.obtain();
                                msg.what = 1003;
                                msg.obj = resultMap;
                                LFReader.this.read_flag = false;
                                LFReader.this.mHandler.sendMessage(msg);
                            }

                        }
                    } catch (IOException var12) {
                        var12.printStackTrace();
                        LFReader.this.read_flag = false;
                        LFReader.this.mHandler.sendEmptyMessage(1002);
                    }
                }
            })).start();
        }
    }

    public InputStream getInputStream() {
        return this.mInputStream;
    }

    public void setInputStream(InputStream mInputStream) {
        this.mInputStream = mInputStream;
    }

    public OutputStream getOutputStream() {
        return this.mOutputStream;
    }

    public void setOutputStream(OutputStream mOutputStream) {
        this.mOutputStream = mOutputStream;
    }

    public boolean stopReadTags() {
        this.read_flag = false;
        return true;
    }

    public synchronized void close() {
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }

        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }

        this.setState(0);
    }

    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private Context context;

        private ConnectThread(BluetoothDevice device, Context context) {
            this.mmDevice = device;
            this.context = context;
            BluetoothSocket tmp = null;

            try {
                tmp = device.createRfcommSocketToServiceRecord(LFReader.MY_UUID);
            } catch (IOException var6) {
            }

            this.mmSocket = tmp;
        }

        public void run() {
            this.setName("ConnectThread");
            LFReader.this.mAdapter.cancelDiscovery();

            try {
                this.mmSocket.connect();
            } catch (IOException var8) {
                try {
                    this.mmSocket = (BluetoothSocket)this.mmDevice.getClass().getMethod("createRfcommSocket", Integer.TYPE).invoke(this.mmDevice, 1);
                    this.mmSocket.connect();
                } catch (IllegalAccessException var4) {
                    var4.printStackTrace();
                    LFReader.this.connectionFailed();
                    return;
                } catch (InvocationTargetException var5) {
                    var5.printStackTrace();
                    LFReader.this.connectionFailed();
                    return;
                } catch (NoSuchMethodException var6) {
                    var6.printStackTrace();
                    LFReader.this.connectionFailed();
                    return;
                } catch (IOException var7) {
                    var7.printStackTrace();
                    LFReader.this.connectionFailed();
                    return;
                }
            }

            synchronized(this) {
                LFReader.this.mConnectThread = null;
            }

            LFReader.this.connected(this.mmSocket, this.mmDevice, this.context);
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (IOException var2) {
            }

        }
    }

    public class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private boolean breakFlag = false;

        public ConnectedThread(BluetoothSocket socket, BluetoothDevice device) {
            this.mmSocket = socket;

            try {
                LFReader.this.mInputStream = socket.getInputStream();
                LFReader.this.mOutputStream = socket.getOutputStream();
            } catch (IOException var5) {
            }

        }

        public void run() {
            boolean dataReceiveFlag = true;
            boolean var2 = false;

            while(true) {
                try {
                    while(true) {
                        if (!LFReader.this.mAdapter.isEnabled() || LFReader.this.mAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                            this.cancel();
                        }

                        if (this.breakFlag) {
                            return;
                        }

                        dataReceiveFlag = true;
                        if (!LFReader.this.read_flag) {
                            sleep(500L);
                        } else {
                            int count;
                            while((count = LFReader.this.mInputStream.available()) == 0) {
                                if (!LFReader.this.read_flag) {
                                    dataReceiveFlag = false;
                                    break;
                                }

                                sleep(200L);
                            }

                            if (dataReceiveFlag) {
                                byte[] buffer = new byte[count];
                                LFReader.this.mInputStream.read(buffer, 0, count);
                                String str_data = new String(buffer, "GB2312");
                                sleep(200L);

                                while((count = LFReader.this.mInputStream.available()) != 0) {
                                    byte[] bufferx = null;
                                    buffer = new byte[count];
                                    LFReader.this.mInputStream.read(buffer, 0, count);
                                    String str = new String(buffer, "GB2312");
                                    str_data = str_data.concat(str);
                                }

                                LFReader.this.parseReceiveString(str_data);
                            }
                        }
                    }
                } catch (IOException var6) {
                    var6.printStackTrace();
                    this.cancel();
                } catch (InterruptedException var7) {
                    var7.printStackTrace();
                    this.cancel();
                } catch (Exception var8) {
                    var8.printStackTrace();
                    this.cancel();
                }
                break;
            }

        }

        private void write(byte[] buffer) {
            try {
                LFReader.this.mOutputStream.write(buffer);
                LFReader.this.mOutputStream.flush();
                Log.i("TESTE", "Vim até aqui!");
            } catch (IOException var3) {
                var3.printStackTrace();
            }

        }

        private void cancel() {
            try {
                this.mmSocket.close();
                LFReader.this.mInputStream = null;
                LFReader.this.mOutputStream = null;
                LFReader.this.read_flag = false;
                this.breakFlag = true;
            } catch (IOException var2) {
            }

        }
    }
}
